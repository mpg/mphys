\section{Heisenberg Model Hamiltonian}

The Heisenberg model is general model to describe interacting spin systems with a Hamiltonian of the form
\begin{equation} \label{heisenberg}
  H = \sum_{i<j}  J_{i,j} (\sigma_i^x \sigma_j^x+\sigma_i^y \sigma_j^y + \eta \sigma_i^x \sigma_j^x)
  +  \epsilon_i \sigma_i^z
\end{equation}
As in the Hubbard model $J_{ij}$ and $\epsilon_i$ are local potentials.  The parameter $\eta$ is anisotropy parameter with two common special cases being the XX-model for $\eta=0$ and the isotropic Heisenberg model for $\eta=1$.   For spin-$\tfrac{1}{2}$ particles the generators $\sigma_i^{x,y,z}$ are tensor products 
\[
   \vec \sigma_i^x = \mathbb{I}^{\otimes i-1} \otimes  \sigma_x \otimes \mathbb{I}^{\otimes N-i}
\]
where $N$ is the total number of spins and the Pauli matrices are
\[ 
\sigma_z = \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}, \quad
\sigma_x = \begin{pmatrix} 0 & 1 \\ 1 &  0 \end{pmatrix}, \quad
\sigma_y = \begin{pmatrix} 0 &-i \\ i &  0 \end{pmatrix} .
\]
It is sometimes convenient to set $\vec \sigma_i = (\sigma_i^x, \sigma_i^y, \sigma_i^z)^T$.  The Heisenberg model is simpler than the Hubbard model because as there is only one mode per site and each mode is two-dimensional.  It is therefore desirable to reduce the spin-Hubbard model to an effective Heisenberg model if possible.

\section{Effective Exchange And Quasi-Spin For 2-Site System}

In the specific case of the Hubbard model describing a chain of two sites,  we can adapt the Hubbard model by taking equation \eqref{hub;ham;2} and setting the values of the site index $i\in\left\{L,R\right\}$ and the site energy between $L$ and $R$ to $\Delta$.
\begin{equation}
\hat H  = \underbrace{J \sum_{\sigma}\left( \hat a_{L,\sigma}^\dag \hat a_{R,\sigma}+ \hat a_{R,\sigma}^\dag \hat a_{L,\sigma} \right)}_{\hat V}
   - \underbrace{\frac\Delta2\sum_{\sigma}\left(\hat n_{L,\sigma}- \hat n_{R,\sigma} \right)
        + U \sum_{i} \hat n_{i,\uparrow}n_{i,\downarrow}}_{\hat H_0}
\end{equation}

We can label the eigenvectors of the number operators and thus of $H_0$ as
\[
  \left|n_{L,\uparrow},n_{L,\downarrow}; n_{R,\uparrow},n_{R,\downarrow}\right>
\in
\left\{
        \overbrace{
        \left|0,0;0,0 \right> 
}^{N = 0}
,\quad
\overbrace{
\begin{matrix}
        \left|0,0;0,1 \right>,\\
        \left|0,0;1,0 \right>,\\ 
        \left|0,1;0,0 \right>,\\ 
        \left|1,0;0,0 \right>,\\ 
\end{matrix}
}^{N=1}
\quad
\overbrace{
\begin{matrix}
        \left|0,0;1,1 \right>,\\ 
        \left|0,1;0,1 \right>,\\ 
        \left|0,1;1,0 \right>,\\ 
        \left|1,0;0,1 \right>,\\ 
        \left|1,0;1,0 \right>,\\ 
        \left|1,1;0,0 \right>,\\ 
\end{matrix}
}^{N=2}
\quad
\overbrace{
\begin{matrix}
        \left|0,1;1,1 \right>,\\ 
        \left|1,0;1,1 \right>,\\ 
        \left|1,1;0,1 \right>,\\ 
        \left|1,1;1,0 \right>,\\ 
\end{matrix}
}^{N=3}
\quad
\overbrace{
        \left|1,1;1,1 \right> 
}^{N=4}
\right\}
\]
and collect them into degenerate subspaces of the total spin operator $\hat N = \sum_\sigma \hat N_\sigma$ as indicated.   These subspaces are closed under the Hamiltonian as described in section X.   We are interested in the $N=2$ subspace
\[
\overbrace{
\begin{matrix}
\left| \Uparrow \right>   = \left|0,1;0,1 \right>,\\
\left| \Downarrow \right> = \left|1,0;1,0 \right>,\\
\end{matrix}
}^{\mbox{Ferromagnetic}}
\quad
\overbrace{
\begin{matrix}
\left| \alpha   \right>   = \left|1,0;0,1 \right>,\\
\left| \beta    \right>   = \left|0,1;1,0 \right>,\\
\end{matrix}
}^{\mbox{Anti-Ferromagnetic}}
\quad
\overbrace{
\begin{matrix}
\left| \gamma_L \right>   = \left|1,1;0,0 \right>\\
\left| \gamma_R \right>   = \left|0,0;1,1 \right>
\end{matrix}
}^{\mbox{Excited}}
\]
which can be split into the following cases
\begin{center}
\begin{tabular}{r l}
        Ferromagnetic: & All spins are aligned. \\ 
        Anti-Ferromagnetic: & The spins alternate. \\
        Excited: &  Multiple spin mode of a single cite are occupied.\\
\end{tabular}
\end{center}

\begin{figure}\centering
  \includegraphics[width=0.6\textwidth]{figures/superexchange.png}
  \caption{Energy-level diagram for second-order tunnelling via super-exchange.} \label{fig:superexchange}
\end{figure}

As the subspace  $\left\{\ket{\alpha}, \ket{\beta}, \ket{\gamma_L},\ket{\gamma_R} \right)$ is closed under $\hat H$,
we can consider a simplified Hamiltonian $\hat H'$ that governs only this subspace.
The energy-level diagram for the subspace is shown in Fig.~\ref{fig:superexchange} and the subspace Hamiltonian is given by
\begin{equation}
        \hat H' = 
\underbrace{
\begin{pmatrix}
        0 & 0 & 0 & 0 \\
        0 & 0 & 0 & 0 \\
        0 & 0 & U - \Delta & 0 \\
        0 & 0 & 0 & U + \Delta \\
\end{pmatrix}
}_{\hat H_0'}
+
\underbrace{
J
\begin{pmatrix}
        0 & 0 & 1 & 1 \\
        0 & 0 & 1 & 1 \\
        1 & 1 & 0 & 0 \\
        1 & 1 & 0 & 0 \\
\end{pmatrix}
}_{\hat V'}
=
\begin{pmatrix}
        0 & 0 & J & J \\
        0 & 0 & J & J \\
        J & J & U - \Delta & 0 \\
        J & J & 0 & U + \Delta \\
\end{pmatrix}
\end{equation}
where the column vectors correspond to $\left(\alpha,\beta,\gamma_L,\gamma_R \right)^T$ .

This effective Hamiltonian can be solved using perturbation theory to find the energy correction to second order
\begin{equation}
  J_{\rm eff} = \frac{J^2}{U+\Delta} + \frac{J^2}{U-\Delta} = \frac{2J^2U}{U^2 - \Delta^2}
\end{equation}
which corresponds to the off-diagonal terms that act to swap spins.  This effect is called second-order super exchange.
Thus an effective two-level system can be written with the Hamiltonian $H_{\rm eff} = J_{\rm eff} \vec \sigma_L \cdot \vec \sigma_R$. 
We would like to generalise this to a larger lattice to obtain of the form
\begin{equation}
H_{\rm eff} = \sum_{\left<i,j\right> }J_{\rm eff} \vec \sigma_i \cdot \vec \sigma_j
\end{equation}

\section{Derivation Of Effective Heisenberg Model For $N>2$}
  
% This simple experimental method provides us a powerful tool to engineer many-body Hamiltonians.

In the general case of atoms trapped in regular optical lattice, the terms proportional to tunneling $J_{\sigma}$ can also be considered via perturbation theory,
in the regime where $J_{\sigma}\ll U_{\sigma},U_{\uparrow \downarrow}$ and $\left\langle n_{i\uparrow}\right\rangle +\left\langle n_{i\downarrow }\right\rangle \simeq 1$,
which corresponds to an insulating phase.

Using a generalization of the Schriffer-Wolf transformation~\cite{hewson1997kondo} to the leading order in $J_{\sigma }/U_{\uparrow \downarrow }$~\cite{duan2003controlling}
showed that equation ~\eqref{hub;ham;2} is equivalent to the following effective Hamiltonian
\begin{equation}
  H_2 =\sum_{\left\langle i,j\right\rangle } \left[ \lambda _z\sigma_{i}^{z}\sigma _{j}^{z}\pm \lambda _{\perp }\left( \sigma _{i}^{x}\sigma_{j}^{x}+\sigma _{i}^{y}\sigma _{j}^{y}\right) \right] 
  + \sum_{i} 4\sqrt{2}\left( J_{\uparrow }^{2}/U_{\uparrow}-J_{\downarrow }^{2}/U_{\downarrow }\right) \sigma_{i}^{z}
\label{Hamiltonian2}
\end{equation}
where $\sigma _{i}^{z}= \tfrac{1}{\sqrt{2}}(n_{i\uparrow }-n_{i\downarrow })$,
$\sigma_{i}^{x}=\tfrac{1}{\sqrt{2}}(a_{i\uparrow }^{\dagger }a_{i\downarrow }+a_{i\downarrow }^{\dagger}a_{i\uparrow })$,
and $\sigma _{i}^{y}=-\tfrac{\imath}{\sqrt{2}}\left( a_{i\uparrow }^{\dagger }a_{i\downarrow }-a_{i\downarrow }^{\dagger }a_{i\uparrow }\right) $
are normalized version of the Pauli spin operators.
The $+$ and $-$ signs before $\lambda _{\perp }$ in equation ~\eqref{lambda} correspond respectively to the cases of fermionic and bosonic atoms.
The parameters $\lambda _z$ and $\lambda _{\perp }$ for the bosonic atoms are given by
\begin{equation}
  \lambda _{z}= \frac{J_{\uparrow}^{2}+ J_{\downarrow }^{2}}{U_{\uparrow \downarrow }}-\frac{2J_{\uparrow}^{2}}{U_{\uparrow }}-\frac{2J_{\downarrow }^{2}}{U_{\downarrow }},
\;\;
\lambda _{\perp }=\frac{2J_{\uparrow }J_{\downarrow }}{U_{\uparrow \downarrow }}.
\label{lambda}
\end{equation}

The Hamiltonian \eqref{hub;ham;2} represents the well-known anisotropic Heisenberg model ($XXZ$ model), which arises in the context of various condensed matter systems \cite{auerbach2012interacting}.  However, the approach involving ultracold atoms has a unique advantage in that the parameters $\lambda _{z}$ and $\lambda_{\perp }$ can be easily controlled by adjusting the intensity of the trapping laser beams.

\section{Special Cases: Isotropic Heisenberg And XX Model}

Let $U_{\uparrow\downarrow}=U$ and assume $J_{\sigma}$ is independent of the spatial direction.  Set $U_{\uparrow }=U_{\downarrow }= \eta U$ and $\beta _{J}=J_{\uparrow }/J_{\downarrow }+J_{\downarrow }/J_{\uparrow}$.
When $J_{\uparrow}$ or $J_{\downarrow }$ is zero then $\lambda_{\perp}=0$ and we have an \emph{Ising model}
and if $\beta _{J}=\pm (1/2-\eta)^{-1}$ we have an SU(2)-symmetric \emph{antiferromagnetic} or
\emph{ferromagnetic system}, respectively.
However, we will not consider these cases here.  

If in addition to the assumptions above $J_\uparrow=J_\downarrow=J$ then 
\begin{equation}
\begin{split}
    H_1 = & -J \sum_{\langle ij\rangle \sigma }\left(a_{i\sigma}^{\dag } a_{j\sigma }+ a_{i\sigma} a_{j\sigma }^\dag \right) 
   + \frac{\eta U}{2} \sum_{i,\sigma } n_{i\sigma }\left( n_{i\sigma}-1\right) 
     + U \sum_{i} n_{i\uparrow} n_{i\downarrow}  \label{Hamiltonian1b}
 \end{split}
\end{equation}
and the effective $J$-coupling terms in the XXZ Heisenberg model are 
\begin{equation}
\lambda _{z} =\frac{2J^2}{U} - \frac{4J^2}{\eta U} = \frac{2J^2}{U} (1- 2\eta^{-1}), \quad
\lambda _{\perp }=\frac{2J^2}{U}.
\label{lambda2}
\end{equation}
$\eta$ controls the anisotropy of the model.
For $\eta=2$ we have $\lambda_z=0$ and $\lambda_\perp = J^2/U$, i.e., Hamiltonian \eqref{hub;ham;2} reduces to an \emph{XX Hamiltonian}. 
If $U_{\sigma }\gg U$, on the other hand, as is the case for Fermionic atoms, we can set $\eta=\infty$,
which yields $\lambda_z = \lambda_\perp = \tfrac{J^2}{U}$ and we have an \emph{isotropic Heisenberg Hamiltonian}.

In the Bosonic case when $\eta=1$ we get $\lambda_z = -\lambda_\perp$ but
there is a negative sign in front of the $\lambda_\perp$ in the
Bosonic case.
Both factors have a minus sign, which is cancelled by the  factor of $-1$ in front.
Thus the Hamiltonian simplifies to an isotropic Heisenberg model with positive coupling again.

In this latter case we can restrict attention to the subspace with at most one atom per mode, i.e., $n_{i\sigma} =0,1$ because the term 

\[\frac{\eta U}{2} \sum_{i,\sigma } n_{i\sigma }\left( n_{i\sigma}-1\right)\] 
vanishes on this subspace while the on-site energies for more than two atoms in the same mode are large.
Restricting to the subspace where $n_{i,\sigma}=0,1$ for all modes, the Hamiltonian reduces to 
\begin{equation}
H_1^{S}=  -J \sum_{\langle ij\rangle \sigma }\left(a_{i\sigma}^{\dag } a_{j\sigma }+ a_{i\sigma} a_{j\sigma}^\dag \right) + U \sum_{i} n_{i\uparrow} n_{i\downarrow}
\label{Hamiltonian1d}
\end{equation}

\section{Effect Of Local Potentials}

The general derivation of the effective Heisenberg Hamiltonian above assumes a regular lattice with no local potentials and imperfections.  Subsequent work attempted to generalize the model.
Start with \eqref{Hamiltonian1e} for a linear chain with nearest neighbour coupling
\begin{equation}
    H_1^{S} =  -J \sum_{j \sigma }\left(a_{j\sigma}^{\dag } a_{j+1,\sigma }+ a_{j,\sigma} a_{j+1,\sigma}^\dag \right) 
    + U \sum_{j} n_{j\uparrow} n_{j\downarrow} 
    + \sum_j \epsilon_j n_{\sigma,j}
\end{equation}

For $N=2$ sites, setting $\epsilon_1=-\frac{1}{2}\Delta$ and $\epsilon_2=+\frac{1}{2}\Delta$ the Hamiltonian becomes explicitly
\begin{equation}
    H_1^{S} =  -J \sum_\sigma \left(a_{1\sigma}^{\dag } a_{2,\sigma }+ a_{1,\sigma} a_{2,\sigma}^\dag \right) 
                      + U (n_{1\uparrow} n_{1\downarrow} + n_{2\uparrow} n_{2\downarrow} )
                      - \frac{\Delta}{2} (n_{1\sigma}-n_{2\sigma})
\end{equation}
and it was shown above that
\begin{equation}
    J_{\rm eff} = \frac{J^2}{U+\Delta} + \frac{J^2}{U-\Delta}.
\end{equation}
A straightforward generalization for a chain with $N>2$ sites and nearest-neighbour interactions suggests
\begin{equation}
  J_{\rm eff,i} = \frac{J^2}{U+\Delta_i} + \frac{J^2}{U-\Delta_i}
\end{equation}
with $\Delta_i = \epsilon_{i+1}-\epsilon_i$.

