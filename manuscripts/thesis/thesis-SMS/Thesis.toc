\contentsline {chapter}{List of Figures}{III}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}The Hubbard Model with Spin}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}The Hubbard Hamiltonian}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Determination of model parameters}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Commutation relations}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Hilbert space dimension}{5}{section.2.4}
\contentsline {chapter}{\numberline {3}The Emergent Heisenberg Model}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Heisenberg model Hamiltonian}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Effective exchange and quasi-spin for 2-site system}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Derivation of effective Heisenberg model for $N>2$}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Special cases: Isotropic Heisenberg and XX model}{10}{section.3.4}
\contentsline {section}{\numberline {3.5}Effect of local potentials}{11}{section.3.5}
\contentsline {chapter}{\numberline {4}Experimental Considerations}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Optical lattice}{13}{section.4.1}
\contentsline {section}{\numberline {4.2}Artifical crystals formed by trapping neutral atoms}{13}{section.4.2}
\contentsline {section}{\numberline {4.3}Creating local potentials}{15}{section.4.3}
\contentsline {section}{\numberline {4.4}Internal `spin' states, control and readout}{15}{section.4.4}
\contentsline {chapter}{\numberline {5}Simulation Results}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Basic simulation approach}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Initial tests to assess numerical stability}{17}{section.5.2}
\contentsline {section}{\numberline {5.3}Simulation task 1: $\alpha $ dependence}{18}{section.5.3}
\contentsline {section}{\numberline {5.4}Simulation task 2: Effect of local potentials}{18}{section.5.4}
\contentsline {section}{\numberline {5.5}Simulation task 3: Lattice Defects and Initialization Errors}{18}{section.5.5}
\contentsline {chapter}{\numberline {6}Conclusion}{19}{chapter.6}
\contentsline {chapter}{Bibliography}{20}{chapter*.10}
\contentsline {chapter}{Appendices}{22}{section*.11}
\contentsline {chapter}{\numberline {A}More about spin and its applications}{23}{Appendix.a.A}
\contentsline {section}{\numberline {A.1}What is spin?}{23}{section.a.A.1}
\contentsline {section}{\numberline {A.2}Exchange interaction}{24}{section.a.A.2}
\contentsline {section}{\numberline {A.3}Spintronics}{25}{section.a.A.3}
\contentsline {chapter}{\numberline {B}Simulation code}{27}{Appendix.a.B}
