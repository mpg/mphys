\contentsline {chapter}{List of Figures}{III}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}The Hubbard Model with Spin}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}The Hubbard Hamiltonian}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Determination Of Model Parameters}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Commutation Relations}{5}{section.2.3}
\contentsline {chapter}{\numberline {3}The Emergent Heisenberg Model}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Heisenberg Model Hamiltonian}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Effective Exchange And Quasi-Spin For 2-Site System}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}Derivation Of Effective Heisenberg Model For $N>2$}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Special Cases: Isotropic Heisenberg And XX Model}{10}{section.3.4}
\contentsline {section}{\numberline {3.5}Effect Of Local Potentials}{11}{section.3.5}
\contentsline {chapter}{\numberline {4}Experimental Considerations}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Optical lattice}{13}{section.4.1}
\contentsline {section}{\numberline {4.2}Artifical crystals formed by trapping neutral atoms}{13}{section.4.2}
\contentsline {section}{\numberline {4.3}Creating local potentials}{15}{section.4.3}
\contentsline {section}{\numberline {4.4}Internal `spin' states, control and readout}{15}{section.4.4}
\contentsline {chapter}{\numberline {5}Simulation Results}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Apparatus}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Basic Simulation Approach}{17}{section.5.2}
\contentsline {section}{\numberline {5.3}Hilbert Space Dimension}{18}{section.5.3}
\contentsline {section}{\numberline {5.4}Matrix Exponential}{20}{section.5.4}
\contentsline {section}{\numberline {5.5}Simulation Specifications}{21}{section.5.5}
\contentsline {section}{\numberline {5.6}Hamiltonians Simulated}{22}{section.5.6}
\contentsline {section}{\numberline {5.7}Discrete Fourier Transform}{23}{section.5.7}
\contentsline {section}{\numberline {5.8}Assessing Numerical Stability}{23}{section.5.8}
\contentsline {section}{\numberline {5.9}Assessing $\alpha $ Dependence}{27}{section.5.9}
\contentsline {section}{\numberline {5.10}Assessing Effect Of Local Potentials}{33}{section.5.10}
\contentsline {chapter}{\numberline {6}Conclusion}{38}{chapter.6}
\contentsline {chapter}{Bibliography}{39}{chapter*.24}
\contentsline {chapter}{Appendices}{41}{section*.25}
\contentsline {chapter}{\numberline {A}More about spin and its applications}{42}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}What is spin?}{42}{section.1.A.1}
\contentsline {section}{\numberline {A.2}Exchange interaction}{43}{section.1.A.2}
\contentsline {section}{\numberline {A.3}Spintronics}{44}{section.1.A.3}
\contentsline {chapter}{\numberline {B}Simulation code}{46}{Appendix.1.B}
