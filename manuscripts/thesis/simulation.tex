\section{Apparatus}
The first option considered was the off-the-self C++ framework QEngine designed for quantum optimal control of ultracold atoms \cite{sorensen2019qengine}.
The framework is extensive and capable but ultimately too unwieldy to be useful for our purposes, it was therefore decided that a bespoke framework should be written.
 
The priority's are that the framework should be simple, extensible, easy to read and therefore decided right, and use mainstream numerical libraries.
It was decided that python fits well because it is already commonly used for scientific computing.
It's syntax is easy to read and write, making the framework more usable and understandable by other researchers.
Python's Numpy library was chosen for the numerical back-end.
Numpy provides a simple API for controlling efficient C routines that manipulate large in-memory multi-dimensional arrays.

\section{Basic Simulation Approach}

The basic approach is to simulate in the Heisenberg picture and observe in the Schrödinger picture.
That is we specify a Hamiltonian $H$ matrix,
then use this to produce a procedure that yields the Unitary matrix $U(t_k)$
for each time $t_k \propto k $,
such that the Unitary matrix satisfies 
\begin{equation}
        U(t_k) = \exp( -\tfrac{i}{\hbar} H  t_k) = \exp( -i\tilde{H} ).
\end{equation}
where $\exp$ denotes the matrix exponential.
For convenience units of time units are chosen such that $t_k = \tfrac{\hbar}{U}k$
\footnote{ The notation confusing but standard. $U$ is the on-site potential, $U(t_k)$ is the Unitary time evolution operator. }
\footnote{Time in such units are labeled {\it Simulation Time}, and their reciprocal {\it Simulation Frequency}.}
making $\tilde{H} = \frac{H}{U}$, where $U$ is a system parameter used latter.

We then use this matrix to calculate the time dependent state  $\ket{t}$ from an initial state $\ket{0}$
$$
\ket{t_k} = U(t_k)\ket{t_0}.
$$

Observations $O_{t_k}$ can then be made by taking the expectation of a time independent matrix $O$ with the time dependent state $\ket{t_k}$
$$
O_{t_k} = \left<O\right>_{t_k} = \left< t_k | O | t_k \right>
$$

\section{Hilbert Space Dimension}
\input{dim.tex}

\section{Matrix Exponential}
The method of simulation requires the computation of the exponential of a matrix.
This is a difficult task in general. 
Symbolic methods exist and are practical for matrices with very small dimension, i.e. $\dim < 5$.

First a digitalisation approach was tyred, where the eigenvectors and eigenvalues of the given matrix are found.
This approach has the benefit of requiring only 1 expensive and error prone step, the digitalisation itself,
after which the matrix exponential becomes a series of scalier exponentials.
However in practice the standard digitalisation algorithm provided by python's Numpy
field to give reliable eigenvectors for matrices of large dimensions, i.e. $\dim > 500$

The next algorithm tyred was the Padé approximation \cite{al2010new} as implemented by python's Scipy in \begin{verbatim} scipy.linalg.expm \end{verbatim}.
This computation is still too expensive, so an method was implemented,
that computes the matrix exponential for the smallest step only,
and computes subsequent unitary operators  by reputedly multiplying by this initial Unitary.
We shall refer to this as the {\bf Iterative Method}.

This is faster then computing the matrix exponential for each time step independently (the {\bf Direct Method})
because matrix multiplication is much cheaper then computing the matrix exponential.
However it is also more error prone because errors from the Padé approximation will combine with errors from
the matrix multiplication and compound by repeated multiplication.
More operations means more errors.

The numerical stability for both methods where investigated in section\ref{sec;numerical stability},
and the iterative method was 
was deemed adequate for exploratory purposes.


\section{Simulation Specifications}
The framework here developed provides primitives and means of composition,
for expressing Hamiltonians in terms the mode annihilation and creation operators, $\hat a_i, \hat a_i^\dag $.
\footnote{The number of levels of each mode is a free parameter, but values above 2 are not explored. This enforces an exclusion principle.}
Also provided is data abstractions for encoding states, and bases.
Operators are functions that map states to states.
A basis is an ordered sequence of states that has an associated mapping called a cast, form the symbolic operators and states to numpy arrays, 2D for operators, 1D for states.
The operators and states have a defined infix addition and multiplication, where multiplication corresponds to function composition
and addition corresponds to addition of the operators image.
Casting is arranged such that the multiplication of two operators becomes the matrix multiplication of the associated arrays
\begin{align*}
Base.Cast \left( A*B(v) \right)
        &= Base.Cast \left( A(B(v) \right)\\
        &= Base.Cast\left(A \right) \times  Base.Cast\left(B \right) \times Base.Cast\left(v \right)
\end{align*}
where $*$ represents the defined infix multiplication $\times$ represents matrix multiplication, $A$,$B$ are operators,$v$ is a state and $Base.Cast$ is the cast method associated with a particular Basis $Base$.
Note if $Base$ is not a subspace of $A$ or $B$, or if $v$  is not in that $Base$, the above computation will fail.
This is a feature to prevent casting to non-invariant subspaces.

This arrangement allows for abstractions to be built in the symbolic layer that is human readable and then cast to a particular subspace
in a particular basis different for efficiently of computation.

A spin chain of length $L$ is represented by chain of modes of length $2L$ where pairs of accent modes such that
$State(0,1)$ represents $\ket{\uparrow}$ and $State(1,0)$ represents $\ket{\downarrow}$. 
Pauli operators are defined as
\begin{align*}
        Z_i &= \hat n_{i,\uparrow} - \hat n_{i,\uparrow} \\
        Y_i &= \hat a^\dag_{i,\uparrow} \hat a_{i,\downarrow} + \hat a^\dag_{i,\downarrow} \hat a_{i,\uparrow} \\
        Y_i &= i(\hat a^\dag_{i,\downarrow} \hat a_{i,\uparrow} - \hat a^\dag_{i,\uparrow} \hat a_{i,\downarrow})
\end{align*}
where $\hat n_{i,\sigma} = \hat a_i^\dag a_{i,\sigma}$

Site occupation number is defined as 
$$
N_i = \hat n_{i,\uparrow} + \hat n_{i,\downarrow}
$$

Once a Hamiltonian has been constructed from the primitive components and cast to a basis,
it can be passed to a unitary-method, along with an array of time values.
The Unitary method can be passed to a generator factory along with an initial state.
The produced generator will return a lazy sequence of states $\ket{t}$. 

Also provided are a number of utilities including an inner product, an expectation function and an expectation function factory.
The Core library is 500 lines, the plotting library used to generate the plots in this report was an additional 350 lines.
The total project including other scripts and library's written in the design and testing phase comes to 3600 lines.
An export version with some dependency eliminated, documentation added along with additional
functionality will be written and is estimated to be 600 lines.


\section{Hamiltonians Simulated}
There is a number of Hamiltonians provided, here we will look at 2.

The Spin-Hubbard Hamiltonian $H=H_1$ and the Heisenberg Hamiltonian $H=H_2$.

The Spin-Hubbard:
\begin{equation}\label{sim;hub}
\tilde{H}_1^{S}=
- \alpha\sum_{\langle ij\rangle \sigma }
\left(\hat a_{i\sigma}^{\dag } \hat a_{j\sigma }+ \hat a_{i\sigma} \hat a_{j\sigma}^\dag \right)
+ \sum_{i}  \left(
        \hat n_{i\uparrow} \hat n_{i\downarrow},
        + \epsilon_i \hat n_{i,\uparrow}
        + \epsilon_i \hat n_{i,\downarrow}
        \right)
\end{equation}
and $\lambda_\perp = \lambda_z = J^2/U = \alpha^2 U$ and $U = 1$.

Heisenberg Hamiltonian:
\begin{equation}\label{sim;his}
\tilde{H}_2 = U^{-1} H_2
        = \alpha^2\sum_{\langle i,j \rangle}
J_{i,j}
(\sigma_i^x \sigma_{j}^x
+ \sigma_i^y \sigma_{j}^y
+ \sigma_i^z \sigma_{j}^z
)
\end{equation}

Where $J_{i,j} = \frac U{ U - \epsilon_i + \epsilon_j } + \frac U{ U  + \epsilon_i - \epsilon_j }$.

\section{Discrete Fourier Transform}
The Discrete Fourier Transform (DFT) is a described version of the Discrete Fourier. 
The DFT is expensive to compute if done naïvely,
however a common algorithm called the Fast Fourier Transform takes advantage of
the symmetries of the Fourier Transform (FFT) to compute the DFT.
The number of symmetries is greatest when the length of the input series is a power of 2,
so at these lengths the FFT is most efficient. \cite{press2007numerical}

Here we use the implementation of the FFT from Python's Numpy  \begin{verbatim} numpy.fft \end{verbatim}.

The sign of the exponent, normalization convention chosen is such that the terms DFT $A_k$ for a series $a_m$ of s are defined 

$$
A_k =  \sum_{m=0}^{n-1} a_m \exp\left\{-2\pi i{mk \over n}\right\}
$$
for $m,k = 1...n-1$.


\section{Assessing Numerical Stability}
\label{sec;numerical stability}
We seek to qualitatively and quantitatively assess the errors that arise from the numerical algorithms used in the simulation.
Error that are consequences of non-physical parameters such as time step, must limit our confidence in any results produced. 

We classify errors into {\it Unitary Errors} and {\it Non-Unitary Errors}.
Unitarity is an important property of the time evolution operator, as it ensures the norm of the initial state is not changed.
Any Unitary matrix can be written as $\exp( -iH )$ where $H$ is Hermitian and vice versa. 
Therefore unitary errors can be understood as perturbations to the Hamiltonian,
whereas Non unitary errors cannot.

We have identified 2 main sources of numerical error: The Padé approximation and the matrix multiplication roughen.
The Padé approximation is known to be error prone and the matrix multiplication is repeated a large number of times so even small errors can be amplified. 

To profile the errors we run the simulation with different computational scenarios and compared them by computing {\it product with reference } for each time step.
The product with reference takes the inner product of the states produced by a simulation with the states of a reference simulation at coinciding times.
This produces a set of values which can be plotted as histogram represented by a mean and standard error.

If the product with reference is taken of the reference then the result is the norm of the time dependent states.
This gives a measure for the non-unitary error.

In table \tableref{table;ns} the norm and product with reference (Product) are simulations run using the iterative method for the same period of time
but with progressively smaller time intervals, and therefore with progressively larger numbers of samples.
The reference is the first simulation in the series, as you can see the norm is the same as the product.
A plot of this data is shown in Figure \ref{fig;5.3}.

\begin{table}\label{table;ns}\begin{center} \begin{tabular}{ c | c c | c c }
Samples & 1 - Product & SEM &  1 - Norm & SEM\\
\hline
10000   & $-3.950 \times 10^{-13}$ & $2.283 \times 10^{-15}$  &  $3.950 \times 10^{-13}$ & $2.283 \times 10^{-15}$ \\
20000   & $ 9.958 \times 10^{-3 }$ & $1.520 \times 10^{-15}$  &  $9.223 \times 10^{-13}$ & $5.353 \times 10^{-15}$ \\
40000   & $ 2.229 \times 10^{-2 }$ & $3.897 \times 10^{-16}$  &  $2.578 \times 10^{-13}$ & $1.392 \times 10^{-15}$ \\
80000   & $ 3.024 \times 10^{-2 }$ & $6.797 \times 10^{-15}$  &  $2.779 \times 10^{-12}$ & $1.603 \times 10^{-14}$ \\
160000  & $ 3.465 \times 10^{-2 }$ & $1.513 \times 10^{-14}$  &  $4.871 \times 10^{-12}$ & $2.819 \times 10^{-14}$ \\
320000  & $ 3.696 \times 10^{-2 }$ & $1.044 \times 10^{-14}$  &  $4.287 \times 10^{-12}$ & $2.468 \times 10^{-14}$ \\
640000  & $ 3.814 \times 10^{-2 }$ & $6.006 \times 10^{-14}$  &  $2.179 \times 10^{-11}$ & $1.263 \times 10^{-13}$ \\
1280000 & $ 3.874 \times 10^{-2 }$ & $2.133 \times 10^{-13}$  &  $7.638 \times 10^{-11}$ & $4.413 \times 10^{-13}$ \\
\end{tabular} \caption{} \end{center} \end{table}


The data in table \ref{tabel;ns} shows that the non-unitary error very small, of the order $10^{-13}$. 
However is shows that for a large number of samples the non-unitary error may be significant.
It is not entirely clear if the increase in product with reference is due to increasing or decreasing error.
As it might be the case that for large sample sizes the Padé approximation is giving more accurate results because
it is computing the exponential of matrices with smaller norm.
This could be investigated by compeering the direct method and the indirect method over a large range of time steps
to see if the errors are arising from the matrix multiplication or from the matrix exponentiation.
The series taken as the reference in table \ref{tabel;ns} is compared against the direct method in Figure \ref{fig;5.3}.
To be more complete this data could be plotted against simulation time and for different time steps.

To gain a richer view of data that underlays table \ref{table;ns} consult the Figure \ref{fig;5.1}
which contains a plot of the product with reference as a histogram and Figure \ref{fig;5.2}
along with the corresponding to the quoted mean and standard error. 
Figure \ref{fig;5.1} plots the histograms on different axes so the variation with in each population can be understood.
It shows that the errors are systematic as the non-zero histogram bins are confined in a very small interval.
It also shows that these values are more or less uniform within this interval, revealing that standard error is an over estimate for the real variation.
Figure \ref{fig;5.2} plots the histograms on the same axis, highlighting that the differences between the series is much larger then the differences within each series.


\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/nummerical_stability_2.pdf}
  \caption{Each plot shows a histogram of the inner product of the states for a particular simulation run with a particular time step,
        with the rates of the reference simulation run at the reference times. Reference (top) is a simulation run with 10000 samples.
        Additional runs use the same system parameters and run for the same length of simulation time but with progressives shorter time steps (halving each time) and thus progressively more samples.
        The histogram of the products with reference of 3 runs are plotted along with Gaussian associated with each data setts mean and standard of error on the mean are plotted.
        (top) reference run, (middle) intermediate run, (bottom) last run.
        Histograms have area 1, and have value 0 outside the range shown for each plot.
        }
  \label{fig;5.1}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/nummerical_stability_3.pdf}
        \caption{Same data as Figure \ref{fig;5.1} however all runs are plotted on the same axes showing the difference between the runs is much greater than the variation within each run.
        The reference curve is the right most peak sitting at 1. }
  \label{fig;5.2}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/nummerical_stability_1.pdf}
        \caption{The mean values of each the product with reference of each run  as shown in Figure \ref{fig;5.2} plotted on the y-axis,
        with the associated number of samples for each run plotted on the x-axis.
        The reference curve is the left most (10000 samples) and highest (mean product with reference of 1)
        showing that it the run is unitary as mean product with reference for the reference run is the mean norm of the states, which is 1.}
  \label{fig;5.3}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/nummerical_stability_0.pdf}
        \caption{ Histograms showing the product with reference and associated Gaussian similar to Figure \ref{fig;5.1}
        where the reference is taken to be the direct method simulate for the same system parameters as before.
        (top) shows product with reference for the reference its self, the direct method.
        (bottom) shows the product with reference of the iterative method for 10000 samples, this run is used as the reference in Figures \ref{fig;5.1}, \ref{fig;5.2} and \ref{fig;5.3}.
        }
  \label{fig;5.3}
\end{figure}



\newpage
\section{Assessing $\alpha$ Dependence}
In this section we compare the Hubbard Hamiltonians and Heisenberg as defined in equation \eqref{sim;hub}  and \eqref{sim;his} respectively for different values of $\alpha$.

When $\alpha = J/U << 1$ The Heisenberg model approximates the Hubbard  model.
We wish to observe and characterise how and when this approximation breaks down.

{\bf First} we consider a typically small value of $\alpha$, $\alpha = 0.01$.
Figure \ref{fig;11} compares the Hubbard and Heisenberg models over a chain of $L=3$ site, for this value of $\alpha$ and no on-site potentials $\epsilon_i = 0$.
There expectation of $N_i$ and $Z_i$, called to Occupation and Magnetisation respectively, are plotted on the y-axis
The left column is the time domain, the right is the frequency domain.
The top row is the first site $i=0$, the second row is the second site $i=1$ and bottom row is the last site $i=3$. 
The initial state is $\ket{\uparrow,\downarrow,\downarrow}$ 

There is a number of observations that require an explanation.

{\bf Observation \ref{fig;11}.1}
{\it Complete agreement.}
The Heisenberg and Hubbard curves lie exactly on top of each other.
This shows that for $\alpha = 0.01$ the Heisenberg model is a very good approximation for the Hubbard model.

{\bf Observation \ref{fig;11}.2}
{\it Constant Occupation.}
The Occupation lines stays at 1 on each site.
The Hubbard Hamiltonian has no state preference because there are no on-site potentials.
Thus by symmetry, we would not expect any Rabi oscillations.
In addition tunneling is suppressed in the Hubbard model because $\alpha$ is small.
The Heisenberg is incapable of producing tunneling under any circumstances.
The initial state $\ket{\uparrow,\downarrow,\downarrow}$ has one atom per site,
the occupation for of each site stats at one.
With no tunneling atoms are confined to each site,
and thus no occupation number is conserved,
the occupation for of each site stays at one.

{\bf Observation \ref{fig;11}.3}
{\it Magnetisation coupling.}
The Magnetisation curves are oscillating implying coupling.
This is to be expected because $\alpha \neq 0$.
If $\alpha = 0 $, then the Heisenberg Hamiltonian would be 0 producing a completely static system,
and the Hubbard Hamiltonian would contain no tunneling term at all and the individual sites would decouple.
As $\alpha \rightarrow 0$ the dynamics in both models become slower, but this is accounted for in the choice of time units.
As $\alpha \rightarrow 0$, the time units $\Delta t \propto \frac1{\alpha^2} \rightarrow \infty$.

{\bf Observation \ref{fig;11}.4}
{\it Constant total Magnetisation.}
For each time, the sum of the Magnetisation in the time domain remains 1
This is harder to see but it is true.
This, along with total conservation in the total occupation number implicit in in Observation \ref{fig;11}.2, is guaranteed from the commutations derived earlier.

{\bf Observation \ref{fig;11}.5}
{\it Discreet Peaks.}
Sites 1 and 3 have 3 peaks, Site 2 has 1 peak. 

{\bf Observation \ref{fig;11}.6}
{\it All peaks align.}
All the Peaks lie on the same Frequencies.

As an implication of Observation 3, the sum of the for all non-zero
frequency the Fourier components of magnetisation and occupation over all the sites must be zero for each time.
Thus no site can have a unique frequency spike.

{\bf Observation \ref{fig;11}.7}
{\it Coincidence of Derivatives in the time domain.}
The turning points, points of inflection and roots in the time domain lie on at the same time values.


{\bf Next} we increase $\alpha$ to the still small value of $0.05$.
Figure \ref{fig;12} shows these systems plotted as before.
Recall that one unit of simulation time is proportional to $1/\alpha^2$ and therefore one unit of time in
Figure \ref{fig;12} represent of $1/25$ of one unit of time in Figure \ref{fig;11}.

{\bf Observation \ref{fig;12}.1} 
{\it Deviation. }
With this choice $\alpha$ the two magnetisation curves for the Hubbard and Heisenberg are sufficiently 
different that they can start to be distinguished in both domains.
However the occupation is still completely coincident.

{\bf Observation \ref{fig;12}.2} 
{\it Similar shapes.}
Both Figure \ref{fig;12} and Figure \ref{fig;11} share a morphology even though the time units have changed by such a dramatic factor.
This is because the factor was chosen to snail with the norm of the Hamiltonians.

{\bf Observation \ref{fig;12}.3} 
{\it Frequency Shift. }
In the spacial domain, the curves are initially coincident,
But has fallen out of sync as the Hubbard line falls behind.
This can be seen as a shift in the spikes in the frequency domain.


{\bf Now} we increase $\alpha$ to the relatively large value of $\alpha = 0.1$.
Figure \ref{fig;13} plots this data as before
except for the Frequency range which has been extended.
With this $\alpha = 0.1$, one unit of time figure \ref{fig;13} is $1/100$ of one unit in Figure \ref{fig;11}.

{\bf Observation \ref{fig;13}.1}
{\it Higher frequency emerge.}
Small shivers have appeared in the Hubbard magnetisation, 
these are apparent as small blips in the frequency domain at around a Simulation Frequency of 8 

{\bf Observation \ref{fig;13}.2}
{\it Substantial frequency shifts.}
The Hubbard magnetisation curves in Figure \ref{fig;13} picks up a significant shift in frequency after 8 Simulation Times.
Because of the increased range of the frequency domain plot this is only apparent in the time domain.

{\bf Observation \ref{fig;13}.3}
{\it Shape Change. }
On site 3, the Hubbard and Heisenberg magnetisation curves have peaks at 
around 2 and 4 Simulation Times.
However the amplitude of these peaks on the Heisenberg curve are the same
but the magnitude of the peaks on the Hubbard curve are different.
In addition to a frequency shift and the addition of shivers,
The Hubbard model in the time domain changes shape.
This indicate that the frequency shifts of the 3 composite peaks on site 3
are not proportional to their frequency position.


{\bf To better understand} the emergence of these deviations, lets return to the $\alpha = 0.05  $ case.
Figure \ref{fig;14} plots the difference between the Hubbard and Heisenberg
in the frequency and time domains as before, where both time and frequency ranges have been adjusted.

{\bf Observation \ref{fig;14}.1}
{\it Amplification. }
The difference is growing as time progresses.
This is the result of the frequency shifts causing the Hubbard curve to fall behind.

{\bf Observation \ref{fig;14}.2}
{\it High frequencies.}
There are extremely high frequency oscillations occurring in the difference.
They can be seen at around a Simulation Frequency of 35.
These High Frequency don't seem to be experiencing the amplification Observation \ref{fig;14}.1.

{\bf We seek} to survey a range of $\alpha$ values.
We note that key observations made by checking the frequency positions of the major peaks in the first sites.
We have implemented a routine that finds the dominant peaks in the frequency domain.
All the peaks grater then some multiple of the maximum peak are picked up.
This multiple is called the threshold.
We Produce Hubbard and Heisenberg Magnetisation curves  for a range of 20 $\alpha$ values,
and then use this routine to find their dominant frequencies, taking threshold to be $1/10$.
Figure \ref{fig;15} is a scatter plot of this data with the Heisenberg model in blue and the Hubbard model in red.
Note that because we have plotted Simulation Frequency on the on the y-axis and $\alpha$ on the x-axis, The frequency in $Hz$ of a peak depends on its x and y position.

{\bf Observation \ref{fig;15}.1}
{\it Constant Heisenberg.}
There are always 3 dominant peaks of the Heisenberg magnetisation, and they 
maintain their frequency in Simulation Frequency,
thus raising in frequency with $\alpha$.


{\bf Observation \ref{fig;15}.2}
{\it Frequency shift.}
The lowest Hubbard peaks coincide with the Heisenberg peaks when alpha is small.
However The Hubbard peaks lower in frequancy giving rise to the 
frequency shift seen in Observation \ref{fig;12}.3 and \ref{fig;13}.2.

{\bf Observation \ref{fig;15}.3}
{\it Relative Frequency change.}
The downward movement of these peaks does not resemble a scaling,
producing the shape change as seen in Observation \ref{fig;13}.3.


{\bf Observation \ref{fig;15}.4}
{\it Emergence of domination high frequency terms }
The higher frequency terms seen in Observation \ref{fig;13}.1 and \ref{fig;14}.2
appear in the Hubbard model for higher $\alpha$ values.
This indicates the higher frequency terms are becoming more significant.

{\bf Observation \ref{fig;15}.5}
{\it Descending  high frequency terms. }
The higher frequency terms seem to descend with $\alpha$.
This in part may be an artifact of the $\alpha$ dependence of Simulation Time.

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E00_A001_STAR_NZ.pdf}
        \caption{Plot of the time and frequency domains for the Hubbard magnetisation (blue), Hubbard occupation (orange), Heisenberg magnetisation (green) and Heisenberg occupation (red).
Note that the Hubbard curves are covered up by the Heisenberg lines, such is the agreement. }
  \label{fig;11}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E00_A005_STAR_NZ.pdf}
        \caption{Plot of the time and frequency domains for the Hubbard magnetisation (blue), Hubbard occupation (orange), Heisenberg magnetisation (green) and Heisenberg occupation (red).
        Note that the Hubbard magnetisation curves start to become visible, in both the time and frequency domains.}
  \label{fig;12}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E00_A010_STAR_NZ.pdf}
        \caption{Plot of the time and frequency domains for the Hubbard magnetisation (blue), Hubbard occupation (orange), Heisenberg magnetisation (green) and Heisenberg occupation (red).
        Note the Hubbard and Heisenberg models significantly fully diverged, as even a subtle frequency shift compounds over time in the time domain}
  \label{fig;13}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E00_A005_DIFF_Z.pdf}
        \caption{Plot of the time and frequency domains of the difference between the magnetisation of the Hubbard and Heisenberg models over time.}
  \label{fig;14}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E00_Arange_Z.pdf}
\caption{Plot showing the trends in major frequency peaks (more then 10\% of the magnitude of the dominant peak) as $\alpha$ is varied.}
  \label{fig;15}
\end{figure}


\section{Assessing Effect Of Local Potentials}


\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E05_A005_STAR_NZ.pdf}
  \label{fig;21}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E05_A005_DIFF_Z.pdf}
  \caption{Plot showing time and frequency domains for the difference between the Hubbard and Heisenberg models for Magnetisation for $\alpha = 0.05$ and site 0 on-site-potential of $\epsilon_0 = 0.5$.}
  \label{fig;22}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_E05_A005_DIFF_N.pdf}
  \caption{Plot showing time and frequency domains for the difference between the Hubbard and Heisenberg models for Occupation for $\alpha = 0.05$ and site 0 on-site-potential of $\epsilon_0 = 0.5$.}
  \label{fig;23}
\end{figure}

\begin{figure}
        \centering
  \includegraphics[width=\linewidth]{figures/L3_Erange_A005_Z.pdf}
        \caption{Plot showing the trends in major frequency peaks (more then 10\% of the magnitude of the dominant peak) in the magnetisation as for $\alpha = 0.05$ as $\epsilon_0$ is varied.}
  \label{fig;24}
\end{figure}



In this section we will compare the Hubbard and Heisenberg models for various values of $epsilon_0$, as the previous section did for $\alpha$.
In order to control the behavior of spinchains, onside potentials are altered, it is thus an important aspect of the model comparison.

We take to be $\alpha = 0.05$, and $\epsilon_i =0$ for $i\neq 0$.

{\bf First} in Figure \ref{fig;21} we repeat the procedure used to generate Figure \ref{fig;12} with the addition of a potential on site 0 or $\epsilon_0 = 0.5$.

{\bf Observation \ref{fig;21}.1}
{\it Frequency shift.}
Similar to what was seen in Observation \ref{fig;12}.3 and \ref{fig;13}.2
The Hubbard magnetisation curve falls behind the Heisenberg magnetisation curve.

{\bf Observation \ref{fig;21}.2}
{\it  Additional Frequency shift.}
Comparing Figure \ref{fig;21} with Figure \ref{fig;12},
We can observe an additional frequency shift up in all the peaks in the frequency domain
of the magnetisation curves of both the Hubbard and Heisenberg curves.

{\bf Observation \ref{fig;21}.3}
{\it Modulation of Site. }
Again compeering Figure \ref{fig;21} with Figure \ref{fig;12},
We can see that a non-proportional shift has occurred similar   
to that seen in Observation \ref{fig;13}.3.

We take a closer look at the difference between the Heisenberg and Hubbard with on-site potential,
by repeating the procedure used to generate Figure \ref{fig;14}
with the on-site potential as used in Figure \ref{fig;21}.

{\bf Observation \ref{fig;22}.1}
{\it Frequency shift.}
As seen in Observation \ref{fig;21}.1
Similar to what was seen in Observation \ref{fig;12}.3 and \ref{fig;13}.2
The Hubbard magnetisation curve falls behind the Heisenberg magnetisation curve.

{\bf Observation \ref{fig;22}.2}
{\it  Additional Frequency shift.}
As seen in Observation \ref{fig;21}.2

{\bf Observation \ref{fig;22}.3}
{\it Modulation of Site. }
As seen in Observation \ref{fig;21}.3

More interestingly, in Figure \ref{fig;23} routine we can repeat the Figure \ref{fig;22} but taking the difference of the occupation curves for the Hubbard and the Heisenberg. 

{\bf Observation \ref{fig;23}.1}
{\it Number per site is no longer conserved.}

In Figure \ref{fig;24} To examine the effect of $\epsilon_0$ we shall repeat the procedure used to create Figure \ref{fig;15} .

{\bf Observation \ref{fig;24}.1}
{\it An increasing trend in the peaks with $\epsilon_0$}
This is to be expected, and is incorporated into the Heisenberg Hamiltonian used.



%\section{Simulation task 3: Lattice Defects and Initialization Errors}
%
%In the ideal case we assume that we have one atom per mode and we start with a well-defined initial state, e.g., $\ket{\uparrow, \downarrow, \downarrow}$ so that $n_{1\uparrow}=n_{2,\downarrow}=n_{3\downarrow}=1$ and $n_{1\downarrow}=n_{2\uparrow}=n_{3\uparrow}=0$.  What happens when the system is not properly initialized?
%
%Start with the simplest case, $N=2$ and assume the initial state is $\ket{\uparrow\downarrow,0}$ or $\ket{\uparrow,0}$ instead of $\ket{\uparrow,\downarrow}$.  Then look at various initial states for $N=3, 4, \ldots$.
%
%ADD RESULTS HERE

