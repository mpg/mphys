\section{The Hubbard Hamiltonian}

The Hubbard model is an extension to the tight binding model that incorporates
on-cite interactions.
Its Hamiltonian is 

\begin{equation}\label{hub;ham;1}
\hat H  
= J \sum_{\left< l, m \right >} \hat a_l^\dag \hat a_m
+ \sum_l \epsilon_l \hat n_l 
+ \frac{U}2 \sum_l \hat n_l(\hat n_l -1)
\end{equation}

Where $\hat a_l$ are mode ladder operators, and $l$ is the mode index.
The ladder operators follow the following commutation relations
$ [\hat a_l, \hat a_j ] = 0$ and $[\hat a_l, \hat a_j^\dag ] = \delta_{ij} $.
We define number operators in the usual way $\hat n = \hat a^\dag \hat a$

We are interested in using this Hamiltonian to model a MOT insulator with internal spin state,
this can be done by assigning two modes to each cite, requiring a total cite per mode of 1 per mode
and ensuring that $J << U$.


This is done by replacing the mode index 
with a lattice index and a spin index
$l \rightarrow (i,\sigma)$
Where $\sigma \in \{\uparrow,\downarrow\}$.
We insure cite occupation is one 
$\hat n_{\uparrow i} + \hat n_{\downarrow i} = \mathbb{I}_i$.

We only consider tunneling between adjacent lattice
cites with identical spin index
\[
\left<(i,\sigma),(j,\sigma')\right> = 
\left\{\left<i,j\right>,\sigma\ \Big |\ \sigma = \sigma' \right\}
\]

\begin{equation}\label{hub;ham;2}
\hat H  
        = \sum_{\left< i, j \right >,\sigma} J_{ij\sigma} \hat a_{i,\sigma}^\dag \hat a_{j,\sigma}
        + \sum_{i,\sigma} \epsilon_{i,\sigma} \hat n_{i,\sigma} 
        + \sum_{i} U_{\uparrow \downarrow} \hat n_{i,\uparrow}n_{i,\downarrow}
        + \sum_{l,\sigma} U_{\sigma} \hat n_l \left( \hat n _l - 1 \right)
\end{equation}

where $J_{ij\sigma} = J_{ji\sigma}^\dag$.

These constants can be computed from physical peramiters

For the cubic lattice and using a harmonic approximation around the minima of the potential \cite{PhysRevLett.81.3108}, the spin-dependent tunnelling energies and the on-site interaction energies are given by 
\begin{align}
    J_{\sigma} &\approx \left(\frac{4}{\sqrt{\pi }}\right) E_{R}^{1/4} \left( V_{\sigma}\right)^{3/4}
    \exp \left[-2 \left(\frac{V_{\sigma }}{E_{R}} \right)^{1/2}\right], \\
   U_{\uparrow\downarrow } &\approx \left(\sqrt{\frac{8}{\pi}} \right) E_{R}^{1/4} (\overline{V}_{\uparrow \downarrow})^{3/4} k a_{s\uparrow \downarrow}
\end{align}
where $E_{R}=\hbar^{2}k^{2}/2m$ is the atomic recoil energy, and  $a_{s\uparrow \downarrow}$ is the scattering length between the atoms of different spins. 
\begin{equation}
\overline{V}_{\uparrow \downarrow } = \frac{ 4V_{\uparrow }V_{\downarrow}} {(V_{\uparrow}^{1/2}+V_{\downarrow}^{1/2})^{2}}
\end{equation}
is the spin average potential in the relevant direction.  For bosonic atoms 
\begin{equation}
U_{\sigma }\approx (8/\pi )^{1/2}\left( ka_{s\sigma }\right) \left( E_{R}V_{\sigma }^3\right) ^{1/4}
\end{equation}
where $a_{s\sigma }$ are the corresponding scattering lengths. For fermionic atoms, $U_{\sigma }$ is on the order of Bloch band separation $\sim 2\sqrt{V_{\sigma }E_{R}}$, which is typically much larger than $U_{\uparrow \downarrow }$ and can be taken to be infinite. 

In writing Eq.~(1), we have neglected overall energy shifts 
\begin{equation}
    \frac{1}{2}\sum_{i}\left( \sqrt{E_{R}V_{\uparrow }}-\sqrt{E_{R}V_{\downarrow }}\right) \left(n_{i\uparrow}-n_{i\downarrow }\right),
    \label{corr1}
\end{equation}
which can be easily compensated by a homogeneous external magnetic field applied in the $z$ direction.  

From the above expressions, we observe that $J_{\sigma }$ depend sensitively (exponentially) upon the ratios $V_{\sigma }/E_{R}$ while $U_{\uparrow \downarrow }$ and $U_{\sigma }$ exhibit only weak dependence.  we can easily \emph{introduce spin-dependent tunneling} $J_{\sigma }$ by varying the potential depth $V_{\uparrow }$ and $V_{\downarrow }$ through control of the intensity of the trapping laser. 

If $V_\uparrow = V_\downarrow = V$ then $V_{\uparrow\downarrow} = V$ and  $J_\sigma=J$ and
\begin{align*}
 J &\approx 4/\sqrt{\pi} \, (E_{R} V^3)^{1/4} \exp \left[-2 \left(\tfrac{V}{E_{R}} \right)^{1/2}\right], \\
   U_\sigma & \approx\sqrt{8/\pi} (E_{R} V^3)^{1/4} k a_{s \sigma}, \\
   U_{\uparrow\downarrow } &\approx \sqrt{8/\pi} (E_{R} V^3)^{1/4} k a_{s\uparrow \downarrow},
\end{align*}
i.e., 
in particular $U_{\uparrow\downarrow}/U_\sigma = a_{s\uparrow\downarrow}/a_{s \sigma}$ and the term \eqref{corr1} vanishes.


\section{Commutation relations}

Notice that $\hat H_0$ is a sum of number operators $\hat n_{i,\sigma}$ which all commute.
This imply that the $\hat H_0$ and $\hat n_{i,\sigma}$ are mutually diagonalisable,
$[\hat H_0,\hat n_{i,\sigma}] = 0$.


The Rerterbation $\hat V$ dose not commute with the number operators
\begin{align*}
        [\hat V, \hat n_{k,\sigma'}] 
      &= \left[J \sum_{\left< i, j \right >,\sigma} \hat a_{i,\sigma}^\dag \hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]
 \\&= J \sum_{\left< i, j \right >,\sigma} \left[\hat a_{i,\sigma}^\dag \hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]
 \\&= J \sum_{\left< i, j \right >,\sigma}\left(
        \hat a_{i,\sigma}^\dag \left[\hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]
      + \left[\hat a_{i,\sigma}^\dag \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]\hat a_{j,\sigma} 
        \right)
 \\&= J \sum_{\left< i, j \right >,\sigma}\left(
        \hat a_{i,\sigma}^\dag  \hat a_{k,\sigma'}
        \underbrace{
        \left[\hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag\right]
        }_{\delta_{jk} \delta_{\sigma\sigma'}}
      + \underbrace{
        \left[\hat a_{i,\sigma}^\dag \comma \hat a_{k,\sigma'}\right]
        }_{-\delta_{ik}\delta_{\sigma\sigma'}}
        \hat a_{k,\sigma'}^\dag \hat a_{j,\sigma} 
        \right)
 \\&= J \sum_{\left< i, j \right >}\left(
        \hat a_{i,\sigma'}^\dag  \hat a_{k,\sigma'}\delta_{jk}
      - \hat a_{k,\sigma'}^\dag \hat a_{j,\sigma'} \delta_{ik}
        \right)
        \neq 0
\end{align*}

But it dose commute with the sum of the number operators $\hat N_\sigma = \sum_{i} \hat n_{i,\sigma}$
\begin{align*}
        [\hat V, \hat N_{\sigma'}] 
        = [\hat V, \sum_k \hat n_{k,\sigma'}]
        = \sum_k [\hat V, \hat n_{k,\sigma'}]
       &= J \sum_{\left< i, j \right >,k}\left(
        \hat a_{i,\sigma'}^\dag  \hat a_{k,\sigma'}\delta_{jk}
      - \hat a_{k,\sigma'}^\dag \hat a_{j,\sigma'} \delta_{ik}
        \right)
     \\&= J \sum_{\left< i, j \right >}\left(
        \hat a_{i,\sigma'}^\dag  \hat a_{j,\sigma'}
      - \hat a_{i,\sigma'}^\dag \hat a_{j,\sigma'} 
        \right)
        =0
\end{align*}

Therefour the total number of each spin is conserved in the boath the unperterbed and fully perterbed Hamiltonian.


\section{Dimention of the Hubbard model}
The components of the hubbard model are the modes and there assosiated ladder operators.
The total hilbert space is the product of the hilbert space for each mode.
The hilbert space of each mode can have a maximum occupation.
The total occupation number per spin is conserved,
therefore we can partition the space into subspaces
with defined total occupation number per spin

We can compute the dimention for the $\hat N$ subspace obaying  $\hat N\left|N\right> =N\left|N\right>$
on a lattice with $L$ cites, this the cite ocupation constraint that $N_i < M$ with 
\begin{equation}\label{dim;gen}
dim_M(N,L) \defeq
	\sum \left(\frac{L!}{\prod(n_i!)_{(n_i\in\mathbb{N}_0)_{i=0}^M}}\right)
	\Bigg|_{
		\sum(n_i)_{i=0}^M = L}
	      ^{
		 \sum(in_i)_{i=0}^M = N}
\end{equation}

With spereret arguments the dimentions for fermions can be fond in a simpeler form.
\begin{align}
	\mbox{Fermions\ \ \ } dim_{1}(N,L) &= \frac{L!}{N!(L-N)!}\label{dim;fermi}\\ 
	\mbox{Boseons\ \ \ } dim_{\infty}(N,L) &= \frac{(N + L - 1)!}{N!(L-1)!}\label{dim;bose}
\end{align}
Equation \eqref{dim;fermi} can easaly obtained by simplifying \eqref{dim;gen},
but this it is not apparent (to me) that \eqref{dim;gen} reduces to \eqref{dim;bose}.
Equating them we have the following strange strange identity

\begin{equation}\label{dim;comp}
	\sum \left(\frac{L!}{\prod(n_i!)_{(n_i\in\mathbb{N}_0)_{i=0}^\infty}}\right)
	\Bigg|_{
		\sum(n_i)_{i=0}^\infty = L}
	      ^{
		 \sum(in_i)_{i=0}^\infty = N}
\equiv \frac{(N + L - 1)!}{N!(L-1)!}
\end{equation}

Note that the upper constraint in equation \eqref{dim;gen} sets to zero
any $n_i$ whose $i > N$, thus $dim_{M\geq N}(N,L) \equiv dim_N(N,L)$ as exspected.

The Denemtion of these subspcaces take off rapidly as seen in figure \ref{fig;dim1}.
The curves demonstrate $dim_{M\geq N}(N,L) \equiv dim_N(N,L)$, and peak at $N = \frac{LM}{2}$.
\begin{figure}
%  \includegraphics[width=\linewidth]{figures/dim1.png}
  \caption{Dimention of a subspace of the hubbard modle for $L=10$, $M=1,2,3,4$ plotted against $N$ }
  \label{fig;dim1}
\end{figure}

\section{Simulation by approximateexact diagonalisation}

I have writen a simulation that uses the Pade approximation for matrix diagonalisation.
The simulation finds the Unitary time dependent operators defined from the Hamiltonian.
\[
U_t = exp\left(-itH\right)
\]

Then finds the time depended state based of an initial state
\[
\left|t\right> = U_t\left|t=0\right>
\]

and finaly computes the expectation of the number operators

\[\left<n_i\right>_t = \left<t\left|\hat a_i^\dag\hat a_i \right| t \right> \]

%\begin{figure}
%  \includegraphics[width=\linewidth]{figures/hub-2.png}
%  \caption{U=1,J=1, 2 cites, flat potential landscape}
%  \label{fig;dim1}
%\end{figure}

%\begin{figure}
%%  \includegraphics[width=\linewidth]{figures/hub-3.png}
%  \caption{U=1,J=1, 3 cites, raised potential in cite 1}
%  \label{fig;dim1}
%\end{figure}

%\begin{figure}
%  \includegraphics[width=\linewidth]{figures/hub-4.png}
%  \caption{U=1,J=1, 4 cites, rased potential at cites 1 and 3}
%  \label{fig;dim1}
%\end{figure}


%\section{Phenomenon of the Hubbard model}
%\section{Direct Simulation}
%\section{Perturbative Approximations}

