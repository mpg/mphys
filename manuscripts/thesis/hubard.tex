\section{The Hubbard Hamiltonian}

The Hubbard model is an extension to the tight-binding model that incorporates on-site interactions described by a Hamiltonian of the form
\begin{equation}\label{hub;ham;1}
\hat H  
= \underbrace{ J \sum_{\left< l, m \right >} \hat a_l^\dag \hat a_m}_{\hat{V}} + \underbrace{\sum_l \epsilon_l \hat n_l  + \frac{U}{2} \sum_l \hat n_l(\hat n_l -1)}_{\hat{H}_0}
\end{equation}
where $\hat a_l$ are mode ladder operators and $l$ is the mode index.  The ladder operators follow the following commutation relations $ [\hat a_l, \hat a_j ] = 0$ and $[\hat a_l, \hat a_j^\dag ] = \delta_{ij} $ and the number operators are defined in the usual way, $\hat n = \hat a^\dag \hat a$ where $J$ is the tunnelling energy, $U$ the on-site interaction energy and $\epsilon_l$ are local potentials.

To be able to describe atoms with internal states in a MOT insulator, this Hubbard Hamiltonian above must be amended include internal degrees of freedom.   This can be done by replacing the mode index with a lattice index and a spin index $l \rightarrow (i,\sigma)$, where $\sigma \in \{\uparrow,\downarrow\}$.  We can ensure site occupation by enforcing $\hat n_{\uparrow i} + \hat n_{\downarrow i} = \mathbb{I}_i$.  This leads to the Hubbard Hamiltonian with spin
\begin{equation}\label{hub;ham;2}
\hat H  
        = \sum_{\left< i, j \right >,\sigma} J_{ij\sigma} \hat a_{i,\sigma}^\dag \hat a_{j,\sigma}
        + \sum_{i,\sigma} \epsilon_{i,\sigma} \hat n_{i,\sigma} 
        + \sum_{i} U_{\uparrow \downarrow} \hat n_{i,\uparrow}n_{i,\downarrow}
        + \sum_{l,\sigma} U_{\sigma} \hat n_l \left( \hat n _l - 1 \right)
\end{equation}
where $J_{ij\sigma} = J_{ji\sigma}^\dag$.  We only consider tunneling between adjacent lattice sites with identical spin index
\[
  \left<(i,\sigma),(j,\sigma')\right> =  \left\{\left<i,j\right>,\sigma\ \Big |\ \sigma = \sigma' \right\}.
\]

\section{Determination Of Model Parameters}

The constants in the model can be computed from physical parameters.  For the cubic lattice and using a harmonic approximation around the minima of the potential~\cite{PhysRevLett.81.3108}, the spin-dependent tunnelling energies and the on-site interaction energies are given by 
\begin{align}
    J_{\sigma} &\approx \left(\frac{4}{\sqrt{\pi }}\right) E_{R}^{1/4} \left( V_{\sigma}\right)^{3/4}
    \exp \left[-2 \left(\frac{V_{\sigma }}{E_{R}} \right)^{1/2}\right], \\
   U_{\uparrow\downarrow } &\approx \left(\sqrt{\frac{8}{\pi}} \right) E_{R}^{1/4} (\overline{V}_{\uparrow \downarrow})^{3/4} k a_{s\uparrow \downarrow}
\end{align}
where $E_{R}=\hbar^{2}k^{2}/2m$ is the atomic recoil energy, and  $a_{s\uparrow \downarrow}$ is the scattering length between the atoms of different spins. 
\begin{equation}
\overline{V}_{\uparrow \downarrow } = \frac{4V_{\uparrow }V_{\downarrow}} {(V_{\uparrow}^{1/2}+V_{\downarrow}^{1/2})^{2}}
\end{equation}
is the spin average potential in the relevant direction.  For bosonic atoms 
\begin{equation}
U_{\sigma }\approx (8/\pi )^{1/2}\left( ka_{s\sigma }\right) \left( E_{R}V_{\sigma }^3\right) ^{1/4}
\end{equation}
where $a_{s\sigma }$ are the corresponding scattering lengths. For fermionic atoms, $U_{\sigma }$ is on the order of Bloch band separation $\sim 2\sqrt{V_{\sigma }E_{R}}$, which is typically much larger than $U_{\uparrow \downarrow }$ and can be taken to be infinite. 

%In writing Eq.~(1), we have neglected overall energy shifts 
%\begin{equation}
%  \frac{1}{2}\sum_{i}\left( \sqrt{E_{R}V_{\uparrow }}-\sqrt{E_{R}V_{\downarrow }}\right) \left(n_{i\uparrow}-n_{i\downarrow }\right),
%   \label{corr1}
%\end{equation}
%which can be easily compensated by a homogeneous external magnetic field applied in the $z$ direction.  

Note that $J_{\sigma }$ depends exponentially on the ratio $V_{\sigma }/E_{R}=\zeta$ while $U_{\uparrow \downarrow }$ and $U_{\sigma }$ exhibit only weak dependence.  Therefore we can easily \emph{introduce spin-dependent tunneling} $J_{\sigma }$ by varying the potential depth $V_{\uparrow }$ and $V_{\downarrow }$ through control of the intensity of the trapping lasers. 

If $V_\uparrow = V_\downarrow = V$ then $V_{\uparrow\downarrow} = V$ and  $J_\sigma=J$ and
\begin{align*}
 J &\approx 4/\sqrt{\pi} \, (E_{R} V^3)^{1/4} \exp \left[-2 \left(\tfrac{V}{E_{R}} \right)^{1/2}\right], \\
   U_\sigma & \approx\sqrt{8/\pi} (E_{R} V^3)^{1/4} k a_{s \sigma}, \\
   U_{\uparrow\downarrow } &\approx \sqrt{8/\pi} (E_{R} V^3)^{1/4} k a_{s\uparrow \downarrow}.
\end{align*}

\section{Commutation Relations}

We can split the Hamiltonian into $\hat{H}_0$ and an interaction term $\hat{V}$.  $\hat H_0$ is a sum of number operators $\hat n_{i,\sigma}$ which all commute.  This imply that the $\hat H_0$ and $\hat n_{i,\sigma}$ are mutually diagonalisable, $[\hat H_0,\hat n_{i,\sigma}] = 0$.  The perturbation $\hat V$ does not commute with the number operators
\begin{align*}
        [\hat V, \hat n_{k,\sigma'}] 
      &= \left[J \sum_{\left< i, j \right >,\sigma} \hat a_{i,\sigma}^\dag \hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]
 \\&= J \sum_{\left< i, j \right >,\sigma} \left[\hat a_{i,\sigma}^\dag \hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]
 \\&= J \sum_{\left< i, j \right >,\sigma}\left(
        \hat a_{i,\sigma}^\dag \left[\hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]
      + \left[\hat a_{i,\sigma}^\dag \comma \hat a_{k,\sigma'}^\dag \hat a_{k,\sigma'}\right]\hat a_{j,\sigma} 
        \right)
 \\&= J \sum_{\left< i, j \right >,\sigma}\left(
        \hat a_{i,\sigma}^\dag  \hat a_{k,\sigma'}
        \underbrace{
        \left[\hat a_{j,\sigma} \comma \hat a_{k,\sigma'}^\dag\right]
        }_{\delta_{jk} \delta_{\sigma\sigma'}}
      + \underbrace{
        \left[\hat a_{i,\sigma}^\dag \comma \hat a_{k,\sigma'}\right]
        }_{-\delta_{ik}\delta_{\sigma\sigma'}}
        \hat a_{k,\sigma'}^\dag \hat a_{j,\sigma} 
        \right)
 \\&= J \sum_{\left< i, j \right >}\left(
        \hat a_{i,\sigma'}^\dag  \hat a_{k,\sigma'}\delta_{jk}
      - \hat a_{k,\sigma'}^\dag \hat a_{j,\sigma'} \delta_{ik}
        \right)
        \neq 0
\end{align*}
but it does commute with the sum of the number operators $\hat N_\sigma = \sum_{i} \hat n_{i,\sigma}$
\begin{align*}
        [\hat V, \hat N_{\sigma'}] 
        = [\hat V, \sum_k \hat n_{k,\sigma'}]
        = \sum_k [\hat V, \hat n_{k,\sigma'}]
       &= J \sum_{\left< i, j \right >,k}\left(
        \hat a_{i,\sigma'}^\dag  \hat a_{k,\sigma'}\delta_{jk}
      - \hat a_{k,\sigma'}^\dag \hat a_{j,\sigma'} \delta_{ik}
        \right)
     \\&= J \sum_{\left< i, j \right >}\left(
        \hat a_{i,\sigma'}^\dag  \hat a_{j,\sigma'}
      - \hat a_{i,\sigma'}^\dag \hat a_{j,\sigma'} 
        \right)
        =0
\end{align*}
Therefore the total number of each spin is conserved in the both the unperterbed and fully perturbed Hamiltonian.

