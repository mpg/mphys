\documentclass{beamer}
\usepackage{graphicx,xcolor,animate}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm,ulem}% bold math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{verbatim}
%\usepackage{blackboard}
\newtheorem{idea}{Idea}
\newtheorem{observation}{Note}
\newtheorem{question}{Question}
\newtheorem{answer}{Answer}
\newtheorem{challenge}{Challenge}
\newtheorem{Solution}{Solution}
\graphicspath{{figures/}}
\DeclareGraphicsExtensions{{.pdf},{.png}}

\def\diag{\operatorname{diag}}
\def\rank{\operatorname{rank}}
\def\Tr{\operatorname{Tr}}
\def\Ad{\operatorname{Ad}}
\def\norm#1{|| #1 ||}
\def\vec#1{\mathbf{#1}}
\def\ave#1{\langle #1\rangle}
\def\ket#1{| #1 \rangle}
\def\bra#1{\langle #1 |}
\def\ip#1#2{\langle #1 | #2 \rangle}
\def\lket#1{| #1 \rangle\rangle}
\def\lbra#1{\langle\langle #1 |}
\def\lip#1#2{\langle\langle #1 | #2 \rangle\rangle}
\def\d{\partial}
\def\op#1{\hat{#1}}
\def\A{\mathcal{A}}
\def\AA{\mathbf{A}}
\def\C{\mathcal{C}}
\def\D{\mathcal{D}}
\def\J{\mathcal{J}}
\def\L{\mathcal{L}}
\def\M{\mathcal{M}}
\def\HH{\mathcal{H}}
\def\DD{\mathfrak{D}}
\def\CP{\mathbb{CP}}
\def\UU{\mathbf{U}}
\def\SU{\mathbf{SU}}
\def\PSU{\mathbf{PSU}}
\def\U{\mathbb{U}}
\def\F{\mathfrak{F}}
\def\G{\mathfrak{G}}
\def\S{\mathcal{S}}
\def\dd{d}
\def\eps{\epsilon}
\def\PAUSE{\pause}
\mode<presentation>{\usetheme{Warsaw}}
\def\LL{\mathfrak{L}}
\def\CP{\mathbf{CP}}
\def\SU{\mathbf{SU}}
\def\U{\mathbf{U}}
\def\up{{\uparrow}}
\def\down{{\downarrow}}
%% other shortcuts
\def\xs{\vec{x}\cdot\vec{\sigma}}
\def\sx{\op{\sigma}_x}
\def\sy{\op{\sigma}_y}
\def\sz{\op{\sigma}_z}
\def\s#1{\op{{\bm \sigma}}^{#1}}
\def\IN{\textsc{in}}
\def\OUT{\textsc{out}}
\def\oo{\omega}
\def\cemph#1{\textcolor{cyan}{#1}}
\mode<presentation>{\usetheme{Warsaw}}
\def\PAUSE{}
\def\PAUSE{\pause}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

%copied from color theme
\definecolor{swanseablue}{RGB}{0,103,182}
\definecolor{swansealblue}{RGB}{133,204,255}
\definecolor{highlightorange}{RGB}{255,160,35}

\setbeamercolor{normal text}{fg=black}
\setbeamercolor{alerted text}{fg=highlightorange}
\setbeamercolor{example text}{fg=swansealblue}
\setbeamercolor{structure}{fg=swanseablue}

\setbeamercolor{author}{fg=swansealblue}
\setbeamercolor{date}{fg=swansealblue}
\setbeamercolor{framesubtitle}{fg=swansealblue}
\setbeamercolor{institute}{fg=swansealblue}

\setbeamercolor{title}{fg=swanseablue}
\defbeamertemplate*{title page}{swansea}[1][]
{
  \vbox{}
  \vfill
  \begin{flushleft}
    \begin{beamercolorbox}[sep=8pt,#1]{title page}
      \usebeamerfont{title}\inserttitle\par%
      \ifx\insertsubtitle\@empty%
      \else%
        \vskip0.25em%
        {\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par}%
      \fi%     
    \end{beamercolorbox}%
    \vskip1em\par
    \begin{beamercolorbox}[sep=8pt,#1]{author}
      \usebeamerfont{author}\insertauthor
    \end{beamercolorbox}
    \begin{beamercolorbox}[sep=8pt,#1]{institute}
      \usebeamerfont{institute}\insertinstitute
    \end{beamercolorbox}
    \begin{beamercolorbox}[sep=8pt,#1]{date}
      \usebeamerfont{date}\insertdate
    \end{beamercolorbox}\vskip0.5em
    \begin{beamercolorbox}[sep=8pt,#1]{institute}
     \usebeamerfont{institute}\conference
    \end{beamercolorbox}\vskip0.5em
    {\usebeamercolor[fg]{titlegraphic}\inserttitlegraphic\par}
    \vskip0.25\paperheight
  \end{flushleft}
 \vfill
}
\setbeamertemplate{title}[swansea]

\title[Modelling cold atoms]{Modelling cold atoms in optical lattices: Hubbard vs Heisenberg model}
\author{Max Gisborne}
\institute{Swansea University}
\date{May 25, 2020}
\def\conference{MPhys Presentation} 

\begin{document}
{\usebackgroundtemplate{\includegraphics[width=\paperwidth,height=\paperheight]{title.pdf}}
\begin{frame} \titlepage \end{frame}}

\begin{frame}{Motivation}

\textbf{Cold atoms in optical lattices} --- interesting platform for
\begin{itemize}
\item exploring physical phenomena \pause
\item testing new control schemes \pause
\item quantum simulation and technology applications \pause
\end{itemize}

% You could break this slide up into two and add a nice picture of the experimental setup

\vfill

Ideal platform to explore control via \textbf{energy landscape shaping} due to significant scope to shape optical potentials using laser and digital mirror devices

\vfill

\textbf{Optimal} control and \textbf{design} of energy landscape for specific applications requires \textbf{models} that are \textbf{accurate yet efficient} to evaluate.

\end{frame}

\begin{frame}{Aims and objectives}

  Hubbard model with spin most accurate description of cold atom dynamics but expensive to evaluate

  Perturbative expansion leads to XXZ coupled spin model --- much more computationally efficent but only applicable in perturbative regime

  In the context of optimal control it is not always easy to know a-priori if we remain in the perturbative regime

  Objective:  Implement simulator for both Hubbard and Heisenberg models to allow for direct comparison of the dynamics and validity of approximations

\end{frame}

\begin{frame}{Hubbard model}

\end{frame}

\begin{frame}{Heisenberg model}

\end{frame}

\begin{frame}{Simulation Approach}

\end{frame}

\begin{frame}{Simulation task 1}

Outline task and present results
  
\end{frame}

\begin{frame}{Simulation task 2}

Outline task and present results
  
\end{frame}

\begin{frame}{Simulation task 3}

Outline task and present results
  
\end{frame}

\begin{frame}{Summary and Outlook}

  What have we learned?
  
  Where next?

  What can we do with the simulator?
  
\end{frame}

\end{document}
