#!/bin/env python
from itertools import product as cart

def repete(foo,init,n):
    yield init
    running = init
    for _ in range(n):
        running = foo(running)
        yield running

def reduce(foo,init):
    def compute(xs):
        y = init
        for x in xs:
            y = foo(y,x)
        return y
    return compute

mull = lambda a,b:a*b
product = reduce(mull,1)
fact = lambda n: product(range(1,n+1)) 


def seq(Ns):
    string = ''
    for i,N in enumerate(Ns):
        string += N*str(i)
    return string

def ExNumber(Ns):
    return sum([mull(a,b) for a,b in enumerate(Ns)])

def Nranges(M,N,L):
    imax = min(M,N) 
    ranges = [range(L+1)]
    ranges.extend([range(N//i + 1) for i in range(1,imax+1)])
    return ranges

def Nss(M,N,L):
    for Ns in cart(*Nranges(M,N,L)):
        if sum(Ns) == L and ExNumber(Ns) == N:
            yield Ns

def denom(Ns): 
    return 1/product(map(fact,Ns))

def denoms(M,N,L):
    return map(denom,Nss(M,N,L))

def dim(M,N,L):
    return fact(L)*sum(denoms(M,N,L))

def dim_inf(N,L):
    return fact(N+L-1)/(fact(N)*fact(L-1))

def dim_1(N,L):
    fact(L)/(fact(N)*fact(L-N))
