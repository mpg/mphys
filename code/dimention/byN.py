#!/bin/env python3
from core import dim,dim_inf,dim_1

import matplotlib.pyplot as plt
import math


rM = range(4)
Ns = list(range(30))
L = 10

renorm = lambda x: x

datas = []
for M in range(1,7):
    datas.append(dict(
        M = M,
        L = L,
        Ns = Ns,
        D = [renorm(dim(M,N,L)) for N in Ns]))
    
datas.append(dict(
    M = '$\infty$',
    L = L,
    Ns = Ns,
    D = [renorm(dim_inf(N,L)) for N in Ns]))


def plot(ax,data):
    ax.scatter(data['Ns'],data['D'],
            label=r'M = {}'.format(data['M']),
            )
    return ax

fig,ax = plt.subplots(figsize=plt.figaspect(0.5))
plt.yscale('log')
ax.grid()
for data in datas:
    plot(ax,data)

plt.title(r'$L = {}$'.format(L))
plt.xlabel(r'Number of exitations $N$')
plt.ylabel(r'dim$_M(N,L)$')
ax.legend()
plt.savefig('dimention.pdf')

plt.show()

