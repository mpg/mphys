from datetime import datetime 
import sys

main_globals = None
log_env_str = 'log_config(main_globals=lambda: globals())'

def log_config(**dic):
    for k in dic:
        globals().update(dic)

LOGFILE = None
SDOUT = True
NO_EVAL_TYPES = {str}

def log_print(*s,bar=False,**ss):
    if LOGFILE and not bar:
        with open(LOGFILE,'a') as f:
            print(*s,**ss,file=f)
    if SDOUT:
        print(*s,**ss)


log = lambda *s,**ss: log_print('-> ',*s,**ss)
defining = lambda *s,**ss: log('[defining]',*s,**ss)
saveing = lambda *s, **ss: log('[saveing]',*s,**ss)
log_update = lambda *s,**ss: log(*s,end='\r',**ss)
log_null = log_print
def perams(**dic):
    log(f'[perameter{"s" if len(dic) > 1 else ""}]')
    args = sys.argv[1:]
    args = {a:b for a,b in zip(args[::2],args[1::2])}
    for k in dic:
        type,data = dic[k]
        if k in args:
            data = args.pop(k)
            if type not in NO_EVAL_TYPES:
                data = eval(data)
        dic[k] = type(data)
        log(f'\t\t{k} = {dic[k]}')
    main_globals().update(args)
    main_globals().update(dic)
    log()

def set_remove(set,obj):
    set.remove(obj)
    return set

def log_bar(name,tot,L=50):
    return lambda i: log(''.join([
                    f'[progress] {name}  [',
                    int(L*i/tot)*'#'+ int(L*(tot-i)/tot)*' ',
                    f'] {round(i,3)}/{round(tot,3)}']),bar=True,end='\r')

COMPS = []

def log_comps(comps=COMPS,file=None):
    log('[Computational Summery]')
    for comp in comps:
        log('\t\t',comp['name'],comp['dt'].total_seconds(), 'seconds')

class computing:
    def __init__(self,name):
        self.name=name
        log('[computing]',self.name)
    def __enter__(self):
        self.t0 = datetime.now()
    def __exit__(self,*stuff):
        dt = datetime.now() - self.t0
        global COMPS
        COMPS.append(dict(name=self.name,t0=self.t0,dt=dt))
