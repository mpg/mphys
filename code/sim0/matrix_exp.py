#!/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
Path.__call__ = lambda self,other: self.joinpath(other).expanduser().resolve().relative_to(self.cwd())
from scipy import linalg
from loging import *
eval(log_env_str) # Dont worry about it but this is an important line
main = __name__ == '__main__'

OUTPUT = f'output/{datetime.now()}'
if 'OUTPUT' in sys.argv:
    i = sys.argv.index('OUTPUT') + 1
    OUTPUT = sys.argv[i]
OUTPUT = Path(OUTPUT)
OUTPUT.mkdir()

log_config(LOGFILE=OUTPUT('log.txt'),NO_EVAL_TYPES={str,Path})
log('started')


array = lambda *vars: np.array(vars)
id2 = np.identity(2)
sum = lambda Xs: np.sum(Xs,axis=0)
roll = lambda Xs: np.roll(Xs,1,axis=0)


def reduce(foo):
    def compute(xs):
        running,xs = xs[0],xs[1:]
        for x in xs:
            running = foo(running,x)
        return running
    return compute

# numpy operations are always biops even when there assosiative
kron = reduce(np.kron) # Kronicker Product of input list
matp = reduce(np.matmul) 
inner = lambda a,b,**s: np.sum(a.conj()*b,**s)
mag = lambda a: inner(a,a)


perams(#peramiter deffinition and logging
OUTPUT= (Path,OUTPUT),
CHAINLENGTH = (int,6),
UP = (np.array,[1,0]),
DOWN = (np.array,[0,1]),
EXITATIONS = (set,{2}),
TIME_LEN = (float,2),
TIME_STEP = (float,0.1),
J = (float,1),
h = (float,1),
MODEL_NAME=(str,'HISENBERG SPIN CHAIN'),
)

log('configuerd')

computing('lattice initial state')
state_initial = kron([[DOWN,UP][i in EXITATIONS] for i in range(CHAINLENGTH)])

plural = lambda O: np.array([
        kron([
                O if i==j else id2
                for i in range(CHAINLENGTH)
            ])
        for j in range(CHAINLENGTH)])

defining('paulis')
X = array([ 0,  1],[  1, 0])
Y = array([ 0,-1j],[ 1j, 0])
Z = array([ 1,  0],[  0,-1])

Xs = plural(X)
Ys = plural(Y)
Zs = plural(Z)

with computing('hamiltonian'):
    H = J*sum(Xs@roll(Xs)+ Ys@roll(Ys) + Zs@roll(Zs)) - h*sum(Xs)
    U = lambda t: linalg.expm(-1j*t*H)

saveing('hamiltonian')
H.tofile(OUTPUT('H.np'))

with computing('unitarys'):
    Us = []
    time = np.arange(0,TIME_LEN,TIME_STEP)
    prog = log_bar('time',time[-1])
    for t in time:
        prog(t)
        Us.append(U(t))

Us = np.array(Us)

saveing('unitarys')
Us.tofile(OUTPUT('U(t).np'))

computing('lattice time deopendent states')
states = Us @ state_initial

with computing('Z projectiions'):
    Zstates = np.array([[
            abs(inner(state,Z @ state))
        for Z in Zs] for state in states])

def show(X,savefile=None,show=False):
    plt.imshow(X)
    if savefile:
        saveing(savefile)
        plt.savefig(savefile)
    if not savefile or show:
        log('showing figure')
        plt.show()

computing('graphics')

show([[inner(state,Z@state).real for Z in Zs] for state in states],
        savefile=OUTPUT('figure.png'),show=False)

log_comps()
log('done')
