#!/bin/bash
rm -rf output/basic
mkdir output/basic
matrix_exp.py\
	OUTPUT "output/basic/No Exitations"\
	TITLE "No Exitations"\
	TIME_LEN 3\
	TIME_STEP 0.01\
	CHAINLENGTH 6\
	J 1\
	H 1 \
	EXITATIONS "{}"

matrix_exp.py\
	OUTPUT "output/basic/No Interaction"\
	TITLE "No Exitations"\
	TIME_LEN 3\
	TIME_STEP 0.01\
	CHAINLENGTH 6\
	J 0\
	H 0 \
	EXITATIONS "{2}"

matrix_exp.py\
	OUTPUT "output/basic/Only J"\
	TITLE "No Exsternal Field"\
	TIME_LEN 3\
	TIME_STEP 0.01\
	CHAINLENGTH 6\
	J 1\
	h 0\
	EXITATIONS "{2}"

matrix_exp.py\
	OUTPUT "output/basic/Only h"\
	TITLE "No J Coupeling"\
	TIME_LEN 3\
	TIME_STEP 0.01\
	CHAINLENGTH 6\
	J 0\
	h 0\
	EXITATIONS "{2}"

matrix_exp.py\
	OUTPUT "output/basic/2 Exitations"\
	TITLE "No J Coupeling"\
	TIME_LEN 3\
	TIME_STEP 0.01\
	CHAINLENGTH 6\
	J 1\
	h 0\
	EXITATIONS "{1,3}"
