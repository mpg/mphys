# Comparison script
# I need to functionalise this script
# The inputs 

import simulator as sim
import numpy as np
import matplotlib.pyplot as plt


CHAINLENGTH = 2
J = 1
U = 100
Delta = 0
J_eff = 1 # (0.63/0.52)*4*2*J**2*U/(U**2 - Delta**2) # <- too be found?
Jxyz = 3*[J_eff]
epsilons = {0:Delta/2,1:-Delta/2,2:Delta/2}

time = np.arange(0,100,0.01)

initial_sim = sim.Down_But([0],CHAINLENGTH)

basis = sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial_sim)
basis_His_Only = sim.one_atom_per_cite(sim.brange(CHAINLENGTH))

cast = sim.mkcast(basis)

Hub_sim = (1/U)* sim.provided_hamiltonions['2'](U,J,{0:Delta/2,},CHAINLENGTH)
His_sim = (1/U)* sim.provided_hamiltonions['hisenberge'](CHAINLENGTH,Jxyz)

initial_np = cast(initial_sim)
Hub_np = cast(Hub_sim)
His_np = cast(His_sim)


print('his')
sim.print_matrix(sim.mkcast(basis_His_Only)(His_sim),basis_His_Only)
print('hub')
sim.print_matrix(cast(Hub_sim),basis)

U = sim.provided_unitary_methods['itteration']
# Useing the itteration U method, given a numpy hamiltoniaon
# and time, It provides a  genorator that yields proporgators for each time.


Simulations = [ # Iterators yielding states
                sim.Compute_States( initial_np, U( Hub_np, time ) ),
                sim.Compute_States( initial_np, U( His_np, time ) )
              ] 



op_ons_sim = tuple( sim.N(i) for i in range(CHAINLENGTH) ) # Oporator, Ocupation number, Plural, Simbolic
op_zms_sim = tuple( sim.Z(i) for i in range(CHAINLENGTH) ) # Oporator, Z-Magnitization , Plural, Simbolic

op_ons_np = tuple(cast(op_on_sim) for op_on_sim in op_ons_sim ) # Oporator, Ocupation number, Plural, Numpy
op_zms_np = tuple(cast(op_zm_sim) for op_zm_sim in op_zms_sim ) # Oporator, Z-Magnitization , Plural, Numpy


# Accumulating lists
hub_ons = [] # hubbard occupation number
his_ons = [] # hisenberg occupation number
hub_zms = [] # hubbard Z magnitization
his_zms = [] # hisenberg Z magnitization

for t,state_hub,state_his in zip(time,*Simulations):

    print( f'{t:.2f}',end='\r')
    hub_on = []
    his_on = []
    hub_zm = []
    his_zm = []

    for i,op in enumerate(op_ons_np):
        hub_on.append( sim.expectation(op, state_hub) )
        his_on.append( sim.expectation(op, state_his) )

    for i,op in enumerate(op_zms_np):
        hub_zm.append( sim.expectation(op, state_hub ) )
        his_zm.append( sim.expectation(op, state_his ) )

    hub_ons.append( hub_on )
    his_ons.append( his_on )
    hub_zms.append( hub_zm )
    his_zms.append( his_zm )

hub_ons = np.array(hub_ons).T
his_ons = np.array(his_ons).T
hub_zms = np.array(hub_zms).T
his_zms = np.array(his_zms).T

#time = np.fft.fftfreq(len(time),time[-1])
# numpy arrays are easyer to dealwith
# expect shape (CHAINLENGTH,TIME_LEN)
#hub_ons = np.abs(np.fft.fft(np.array(hub_ons).T,axis=1))[:,time > 0]
#his_ons = np.abs(np.fft.fft(np.array(his_ons).T,axis=1))[:,time > 0]
#hub_zms = np.abs(np.fft.fft(np.array(hub_zms).T,axis=1))[:,time > 0]
#his_zms = np.abs(np.fft.fft(np.array(his_zms).T,axis=1))[:,time > 0]

#time = time[time > 0]

fig,axs = plt.subplots(CHAINLENGTH+1,2)
axsl,axsr = axs.T

for i,ax in enumerate(axsl):
    ax.grid()
    if i == CHAINLENGTH:
        ax.plot(time,np.sum(hub_ons,axis=0))
        ax.plot(time,np.sum(hub_zms,axis=0))
        continue

    ax.plot(time,hub_ons[i])
    ax.plot(time,hub_zms[i])


for i,ax in enumerate(axsr):
    ax.grid()
    if i == CHAINLENGTH:
        ax.plot(time,np.sum(his_ons,axis=0))
        ax.plot(time,np.sum(his_zms,axis=0))
        continue

    ax.plot(time,his_ons[i])
    ax.plot(time,his_zms[i])


plt.show()
