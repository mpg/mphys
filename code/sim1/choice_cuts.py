from moap import *
import sys

dT = 0.01
LT = 500

# figure 5.8
args = HHNZ(3,0.05,0,np.arange(0,LT,dT))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
print(peram_name)
fig = Plot_TDFD(DIFF(Z(args)),fig_args = dict(figsize = plt.figaspect(0.5)), max_time = 10, max_freq = np.inf)
fig.suptitle(r'$\alpha = 0.05$')
fig.savefig(f'results/choice_cuts/L3_E00_A005_DIFF_Z.pdf')

#figure 5.11
args = HHNZ(3,0.05,0.5,np.arange(0,LT,dT*0.1))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
print(peram_name)
fig = Plot_TDFD(DIFF(Z(args)),fig_args = dict(figsize = plt.figaspect(0.5)), max_time = 8, max_freq = 0.6)
fig.suptitle(r'$\alpha = 0.05, \epsilon_0 = 0.5$')
fig.savefig(f'results/choice_cuts/L3_E05_A005_DIFF_Z.pdf')

#figure 5.12
args = HHNZ(3,0.05,0.5,np.arange(0,LT,dT))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
print(peram_name)
fig = Plot_TDFD(DIFF(N(args)),fig_args = dict(figsize = plt.figaspect(0.5)), max_time = 8, max_freq = 0.6)
fig.suptitle(r'$\alpha = 0.05, \epsilon_0 = 0.5$')
fig.savefig(f'results/choice_cuts/L3_E05_A005_DIFF_N.pdf')

# figure 5.10
args = HHNZ(3,0.05,0.5,np.arange(0,LT,dT))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
print(peram_name)
fig = Plot_TDFD(STAR(NZ(args)),fig_args = dict(figsize = plt.figaspect(0.5)),max_time = 8)
fig.suptitle(r'$\alpha = 0.05, \epsilon_0 = 0.5$')
fig.savefig(f'results/choice_cuts/L3_E05_A005_STAR_NZ.pdf')

# figure 5.7
args = HHNZ(3,0.1,0,np.arange(0,LT,dT))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
fig = Plot_TDFD(STAR(NZ(args)),fig_args = dict(figsize = plt.figaspect(0.5)), max_time = 8, max_freq = 10)
fig.suptitle(r'$\alpha = 0.1$')
fig.savefig(f'results/choice_cuts/L3_E00_A010_STAR_NZ.pdf')

# figure 5.6
args = HHNZ(3,0.05,0,np.arange(0,LT,dT))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
print(peram_name)
fig = Plot_TDFD(STAR(NZ(args)),fig_args = dict(figsize = plt.figaspect(0.5)),max_time = 8)
fig.suptitle(r'$\alpha = 0.05$')
fig.savefig(f'results/choice_cuts/L3_E00_A005_STAR_NZ.pdf')


# figure 5.5
args = HHNZ(3,0.01,0,np.arange(0,LT,dT))
peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
print(peram_name)
fig = Plot_TDFD(STAR(NZ(args)),fig_args = dict(figsize = plt.figaspect(0.5)),max_time = 8)
fig.suptitle(r'$\alpha = 0.01$')
fig.savefig(f'results/choice_cuts/L3_E00_A001_STAR_NZ.pdf')


# figure 5.9

peaks_N_hub = []
peaks_N_his = []
peaks_Z_hub = []
peaks_Z_his = []
number_of_points = 20
min_point,max_point = 0.01,0.5
for eps in [i*(max_point - min_point)/(number_of_points-1) + min_point for i in range(number_of_points)]:
    args = HHNZ(3,0.05,eps,ref_time = np.arange(0,2*LT,dT))
    peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
    print(peram_name)
    peaks_N_hub.append( PEAKS(FFT(CHUB(N(args)))) )
    peaks_N_his.append( PEAKS(FFT(CHIS(N(args)))) )
    peaks_Z_hub.append( PEAKS(FFT(CHUB(Z(args)))) )
    peaks_Z_his.append( PEAKS(FFT(CHIS(Z(args)))) )
    

fig = Plot_Peaks('eps',peaks_Z_hub + peaks_Z_his,fig_args = dict(figsize = plt.figaspect(0.5)))
fig.suptitle(r'$\alpha = 0.05$, Threshold $ = 1/10$, site 1 of 3')
fig.savefig(f'results/choice_cuts/L3_Erange_A005_Z.pdf')


# figure 5.11

peaks_N_hub = []
peaks_N_his = []
peaks_Z_hub = []
peaks_Z_his = []
number_of_points = 20
min_point,max_point = 0.01,0.4
for alpha in [i*(max_point - min_point)/(number_of_points-1) + min_point for i in range(number_of_points)]:
    args = HHNZ(3,alpha,0,ref_time = np.arange(0,2*LT,dT))
    peram_name = 'L{L}A{alpha}E{eps}'.format(**args[1])
    print(peram_name)
    peaks_N_hub.append( PEAKS(FFT(CHUB(N(args)))) )
    peaks_N_his.append( PEAKS(FFT(CHIS(N(args)))) )
    peaks_Z_hub.append( PEAKS(FFT(CHUB(Z(args)))) )
    peaks_Z_his.append( PEAKS(FFT(CHIS(Z(args)))) )
    

fig = Plot_Peaks('alpha',peaks_Z_hub + peaks_Z_his,fig_args = dict(figsize = plt.figaspect(0.5)))
fig.suptitle(r'Threshold $ = 1/10$, site 1 of 3')
fig.savefig(f'results/choice_cuts/L3_E00_Arange_Z.pdf')


plt.show()
sys.exit()
