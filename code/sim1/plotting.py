from loging import *
import pyrsistent as pyr
import peramiters as peram
import sys
from math import inf

scopes = {}

def reduce(foo):
    def compute(xs):
        xs = list(xs)
        running,xs = xs[0],xs[1:]
        for x in xs:
            running = foo(running,x)
        return running
    return compute

compose = reduce(lambda x,y: lambda z: x(y(z)))

# Altair Section

def setup_alt(data):
    log('importing altair')
    globals()['alt'] = import_from_path('altair')
    alt.renderers.enable('altair_viewer')
    alt.data_transformers.enable('data_server')
    return data


def plot_altair_all(data):
    # config pased with the let binding
    td = data.Time
    chart_td = alt.Chart(td).mark_line().encode(
            x = 'Time',
            y = 'Value',
            color = 'Obs',
            row = 'Cite',
            ).interactive()

    fd = data.Freq
    fd = fd.loc[fd.Freq > 0]
    fd = fd.loc[fd.Part == 'Magnitude']
    chart_fd = alt.Chart(fd).mark_line().encode(
            x = 'Freq',
            y = 'Value',
            shape = 'Obs',
            #color = 'Part',
            row = 'Cite',
            ).interactive()

    if DEBUG:
        global scopes
        scopes.append['alt':locals()]

    return chart_td | chart_fd


    if DEBUG:
        scopes.update(plot1=locals())

def taredown_alt(chart):
    log('displaying chart')
    return chart.show()

# Matplotlib Section

def setup_mpl(data):
    log('importing matplotlib')
    globals()['plt'] = import_from_path('matplotlib.pyplot')

    _tmp = {domain:{cite:{model:{obs:None\
                                   for obs in set(data[domain].Obs)}
                                   for model in set(data[domain].Model)}
                                   for cite in set(data[domain].Cite)}
                                   for domain in data}
    for domain in data:
        df = data[domain]
        for cite in set(df.Cite):
            for model in set(df.Model):
                for obs in set(df.Obs):
                    _df = df.loc[(df.Obs == obs) & (df.Cite == cite) & (df.Model == model)]
                    if domain == 'Freq':
                        _df = _df.loc[(_df.Part == 'Magnitude')]
                    _tmp[domain][cite][model][obs] = (_df[domain],_df.Value)

    return freeze(_tmp)


def plot_mpl_simple(data):
    log('simple')

    fig,axss = plt.subplots(config.CHAINLENGTH,2)
    for axs in axss:
        for ax in axs:
            ax.grid()

    axsl,axsr = list(zip(*axss))
    cites = list(data.Time)
    models = list(data.Time[0])

    for cite,axl,axr in zip(cites,axsl,axsr):
        for model in models:
            TCite = data.Time[cite][model]
            FCite = data.Freq[cite][model]

            if DEBUG:
                scopes.update(plot1=locals())

            axl.plot(*TCite.Num)
            axl.plot(*TCite.Mag)
            axr.plot(*FCite.Num)
            axr.plot(*FCite.Mag)


    if DEBUG:
        scopes.update({'mpl':locals()})
    
    return fig,(*axsl,*axsr)


def taredown_mpl(fig_axs):
    fig,axs = fig_axs
    log('write plot',config.WRITE_PLOT)
    if config.WRITE_PLOT:
        log('saveing figure')
        fig.savefig(OUTPUT('fig.png'))
    if config.SHOW_PLOT:
        log('displaying figure')
        plt.show()
    log('mpl exiting')
 
# Plotting Switch Section

lib_defult = {
        'alt': 'all',
        'mpl':'simple'
        }

lib_plottings = {
        'alt': { 'all': plot_altair_all },
        'mpl': { 'simple':plot_mpl_simple }
        }

lib_setup = {
        'alt': setup_alt,
        'mpl': setup_mpl,
        }

lib_tardown = {
        'alt': taredown_alt,
        'mpl': taredown_mpl,
        }

# Data Prossessing Section

@arg_factory(eval,eval)
def pipe_freq_range(fmin=0,fmax=inf):
    def pipe(data):
        return data.set('Freq',data.Freq.loc[
                                          (data.Freq.Freq < fmax)
                                        & (data.Freq.Freq > fmin)
                                        ].sort_values('Freq'))
    return pipe


pross_pipes = {
        '$': lambda *_ : lambda x:x,
        'freq_range': (pipe_freq_range),
        }

pross_defult_sorc = '$'

def data_pipe_line(lib,pipe_spec,config):
    configer = let(config = config)

    pipe_spec = list(reversed(pipe_spec)) 

    sync = pipe_spec[0]
    sorc = pipe_spec[-1]

    if sorc == '':
        pipe_spec[-1] = pross_defult_sorc

    if sync == '':
        plot_choice = lib_defult[lib]

    else:
        plot_choice = sync

    if plot_choice not in lib_plottings[lib]:
        print('')
        print(f'un-recognised plot "{plot_choice}"',end='\n\n')
        print('Potentialy missing trailing "|" in PLOT specification',end='\n\n')
        print(f'The PLOT spec is defines the data pipeline, the last in the pipline is the plotting function. If none if given a defult is used, but if you dont wright "|" then the last prossessing argument, in this case "{plot_choice}" will be interpreted as the plotting roughteen')
        sys.exit(1)

    plot = lib_plottings[lib][plot_choice]

    pipe_line = [ section.split() for section in pipe_spec[1:] ]
    log('data pipe-line = ',' | '.join([' '.join(sec) for sec in list(reversed(pipe_line))] + [str(plot_choice)]))


    for i,section in enumerate(pipe_line):
        pip_name,*args = section
        pipe_line[i] = pross_pipes[pip_name](*args)
        assert callable(pipe_line[i]), f'pipe section "{pipe_spec[1:][i]}" is not callable!'
        
    if DEBUG:
        scopes.update({'pipe-line':locals()})

    return configer(compose([lib_tardown[lib],
                     plot,
                     lib_setup[lib],
                     *pipe_line]))


    return prossess

# Mecheanary Section

def main(**kwargs):
    if __name__ == '__main__':
        args = None # peramiters will be read in from the call line
        defult_load = True
    else:
        args = kwargs # peramiters will be set by the calling script 
        defult_load = False
        if 'log' in args: # Inline loggnig if provided by calling script
            global log
            _log = args['log']
            log = lambda *s,**ss : _log('[plot]',*s,**ss)

    Config_Main = peram.Main_Plot(args,log,defults={'LOAD':defult_load})
    Config_Plot = peram.Ploting(args,log)
    Config_System = peram.Hyper(args,log) + peram.Phys(args,log)

    if Config_Plot.PLOT == 'no':
        sys.exit()

    if not Config_Main:
        sys.exit()

    if Config_Main.LOAD:
        if Config_Main.LOAD_PATH == Path('/dev/null'):
            try:
                Config_Main = Config_Main.set('LOAD_PATH',next(Path('output').glob('*'))) # grab the first directory that matches output/*
            except StopIteration:
                print('No files in the "output" directory to load')
                sys.exit(1)


        get_config = lambda x: slurp(Config_Main.LOAD_PATH(x+'.pickle'))
        Config_System = get_config('Hyper') + get_config('Physics')
        
        data = freeze(dict(
        Time = slurp(Config_Main.LOAD_PATH('time_domain.pkl')),
        Freq = slurp(Config_Main.LOAD_PATH('frequancy_domain.pkl'))
        ))

    else:
        assert 'data' in args, 'no load specifyed -> data muset be passed with the "data" key word'
        data = args['data']

    OUTPUT = Config_Main.LOAD_PATH(Config_Plot.PLOT_DIR)
    if not OUTPUT.exists():
        OUTPUT.mkdir()

    Config_Main = Config_Main.set('OUTPUT',OUTPUT)

    if DEBUG:
        global scopes
        scopes.update(main=locals())
        globals().update(locals())


    lib,*pipe_spec = Config_Plot.PLOT
    troff = data_pipe_line(lib,pipe_spec,config = Config_Plot + Config_System)(data)

    if DEBUG:
        scopes.update(main=locals())
        globals().update(locals())


if __name__ == '__main__':
    DEBUG = True
    main()
else:
    DEBUG = False
