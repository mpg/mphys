import simulator as sim
import numpy as np
from helpers import Acuumulator, trunkate
import matplotlib.pyplot as plt

scope = None

def H1S(
        alpha = 0.1,
        CHAINLENGTH = 3,
        time = np.arange(0,200,0.01),
        epsilons = {},
        ):


    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial)
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    H_sim = (1/(2*alpha**2))*sim.provided_hamiltonions['2'](1,alpha,epsilons,CHAINLENGTH)

    H = cast(H_sim)

    print(alpha)
    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)

    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    return time,Ns.arraysT(),Zs.arraysT()

def H2(
        alpha = 0.1,
        CHAINLENGTH = 3,
        time = np.arange(0,200,0.01),
        ):

    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.one_atom_per_cite(sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial))
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    Jeff = 2*alpha**2
    H_sim = (1/(2*alpha**2))*sim.provided_hamiltonions['hisenberge'](CHAINLENGTH,3*[Jeff])


    H = cast(H_sim)

    print(alpha)
    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)

    
    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    return time,Ns.arraysT(),Zs.arraysT()

def fft(time,*series):
    time,*series = trunkate(time,*series)
    freq = np.fft.fftfreq(len(time),time[1]) 
    range = freq > 0
    freq = freq[range]
    tran = [np.abs(np.fft.fft(s)[range]) for s in series]
    return (freq,*tran)

def peak(freq,amp):
    max_amp = max(amp)
    dif = np.sign(np.diff(amp)+1)
    diff = np.diff(dif)
    d = (np.abs(diff) > 1.5) & ( amp[1:-1] > max_amp/10 )
    global scope
    scope = locals()
    return freq[1:-1][d]


def plot(*curlves):
    fig,axs = plt.subplots(len(curlves[0][1]))
    [ax.grid() for ax in axs]
    for time,Zs in curlves:
        print(len(time))
        for i,ax in enumerate(axs):
            ax.plot(*trunkate(time,Zs[i]))
    return fig

def peak(freq,amp):
    max_amp = max(amp)
    dif = np.sign(np.diff(amp)+1)
    diff = np.diff(dif)
    d = (np.abs(diff) > 1.5) & ( amp[1:-1] > max_amp/10 )
    global scope
    scope = locals()
    return freq[1:-1][d]


CHAINLENGTH = 3
ref_time = np.arange(0,2000,0.01)

def emergent_plot(a = 0.01):
    hub_peaks = []
    his_peaks = []
    resids = []
    hub_time, hub_Ns, hub_Zs = H1S( a, CHAINLENGTH, time = ref_time )
    his_time, his_Ns, his_Zs = H2( a, CHAINLENGTH, time = ref_time )

    print('fft')
    hub_freq,*hub_fft = fft(hub_time,*hub_Zs)
    his_freq,*his_fft = fft(his_time,*his_Zs)

    hub_peak = peak(hub_freq,hub_fft[0])
    his_peak = peak(his_freq,his_fft[0])

    tuple(hub_peaks.append( (a,f) ) for f in hub_peak) 
    tuple(his_peaks.append( (a,f) ) for f in his_peak)

    running = 0
    for hub,his in zip(hub_peak,his_peak):
        running += (hub - his) **2
    running = np.sqrt(np.sum(running))/3
    resids.append((a,running))

    def limmet(time,limet):
        return time[time < limet]

    plot(
            (limmet(hub_time,5), hub_Zs),
            (limmet(his_time,5), his_Zs),
            (limmet(hub_time,5), hub_Ns),
            (limmet(his_time,5), his_Ns)
            ).savefig(f'results/emergent/ontopof_alpha{a}_time.pdf')

    plot(
            (limmet(hub_freq,0.5), hub_fft),
            (limmet(his_freq,0.5), his_fft),
            ).savefig(f'results/emergent/ontopof_alpha{a}_freq.pdf')


emergent_plot(a = 0.2)
emergent_plot(a = 0.1)
emergent_plot(a = 0.05)
plt.show()
