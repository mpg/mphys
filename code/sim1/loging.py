from datetime import datetime 
import sys
import pickle
from pyrsistent import freeze
from pathlib import Path
Path.__call__ = lambda self,other: self.joinpath(other).expanduser().resolve().relative_to(self.cwd())


main_globals = None
log_env_str = 'log_config(main_globals=lambda: globals())'


import importlib
def import_from_path(module_name):
    return importlib.import_module(module_name)

import imp
def import_local(module_name):
    return imp.load_module(module_name,*imp.find_module(module_name,[Path('.')]))


def log_config(**dic):
    for k in dic:
        globals().update(dic)

LOGFILE = None
SDOUT = True

def let(**binding):
    def decorator(f):
        def inner(*args,**kwargs):
            g = f.__globals__
            old = {}
            for key in binding:
                if key in g:
                    old.update({key:g[key]})
                g[key] = binding[key]
            result = f(*args,**kwargs)
            g.update(old)
            for key in set(binding) - set(old):
                del g[key]
            return result
        return inner
    return decorator


def arg_factory(*pa_spec, **kwa_spec):
    def decorator(function):
        def inner(*pa,**kwa):
            _pa,_kwa = [],{}
            for a,factory in zip(pa,pa_spec):
                _pa.append(factory(a))

            for kw in kwa:
                a = kwa[kw]
                factory = kwa_spec[kw]
                _kwa.update({ kw: factory(a) })

            return function(*_pa,**_kwa)
        return inner
    return decorator


def log_print(*s,bar=False,**ss):
    if LOGFILE and not bar:
        with open(LOGFILE,'a') as f:
            print(*s,**ss,file=f)
    if SDOUT:
        print(*s,**ss)


log = lambda *s,**ss: log_print('-> ' if 'end' not in ss else '',*s,**ss)
defining = lambda *s,**ss: log('[defining]',*s,**ss)
saveing = lambda *s, **ss: log('[saveing]',*s,**ss)
log_update = lambda *s,**ss: log(*s,end='\r',**ss)
log_null = log_print

def log_keep(path,obj):
    with open(OUTPUT(path),'wb') as file:
        pickle.dump(obj,file) 
def slurp(path):
    with open(path,'rb') as file:
        return pickle.load(file)

def tee(path,obj):
    with open(OUTPUT(path),'wb') as file:
        pickle.dump(obj,file)
    return obj


def set_remove(set,obj):
    set.remove(obj)
    return set

def log_bar(name,tot,L=50):
    return lambda i: log(''.join([
                    f'[progress] {name}  [',
                    int(L*i/tot)*'#'+ int(L*(tot-i)/tot)*' ',
                    f'] {round(i,3)}/{round(tot,3)}']),bar=True,end='\r')

COMPS = []

def log_comps(comps=COMPS,file=None):
    log('[Computational Summery]')
    for comp in comps:
        log('\t\t',comp['name'],comp['dt'].total_seconds(), 'seconds')

class computing:
    def __init__(self,name):
        self.name=name
        log('[computing]',self.name)
    def __enter__(self):
        self.t0 = datetime.now()
    def __exit__(self,*stuff):
        dt = datetime.now() - self.t0
        global COMPS
        COMPS.append(dict(name=self.name,t0=self.t0,dt=dt))
