from loging import *

OUTPUT = '/dev/null'

NO_EVAL_TYPES = {Path,str}
peram_source = sys.argv[1:]
peram_source = {a:b for a,b in zip(peram_source[::2],peram_source[1::2])}

NO_EVAL = 'NO EVAL'

def perams(maindoc,**dic):
    def get(args = None,
            log = log,
            defults = {},
            no_eval_types = NO_EVAL_TYPES,
            tendency_to_eval = False):
        if not args:
            log('reading arguments from call line')
            tendency_to_eval = True
            args = peram_source.copy()
            call_line = list(args)
            call_line.extend([args[k] for k in args])

            if '--help' in call_line:
                for key in dic:
                    print('=| ',str(key)+'::'+str(dic[key][0]))
                    print('=| ','Defult Value:',dic[key][1])
                    print('=| ','Doc:',dic[key][2])
                    print('')
                    return None

        log(f'[peram]')

        _dic = {}
        for k in dic:
            arg_type,data,doc,*others = dic[k]
            no_eval = False
            if len(others) > 0:
                no_eval = others[0] == NO_EVAL
            if k in defults:
                data = defults[k]
            doc = doc or str(k)+'::'+str(arg_type)
            if k in args:
                data = args.pop(k)
                if tendency_to_eval and not no_eval and arg_type not in no_eval_types and type(data) == str:
                    data = eval(data)
            _dic.update({k: arg_type(data)})
            log(f'\t\t{k} = {_dic[k]}')
        #args.update(dic)
        #main_globals().update(args)

        return freeze(_dic)
    return get


# perams defines both comand line and python interfaces.
# perams return a peramiter factory
# the peramiter factory returns a Persistant Imutable Dictionarys called PMaps provided by pyrsistent 
#               There enterys can be acsessed by dot notiation
# keyvalue arguments of perams defines command line arguments
# key defines peramiter name 
# values are a 3 tuple (type,defult,documentation)
# if key is present in call line, eval will be applyed followed by type.
# NO_EVAL_TYPES contains type, then that type will not have eval called 
# the sorce of peramiter may be specifyed by the user, as the "arg" key word argument to the peramter factory.
# If no "arg" is given then the peramiter factory will read peramiters from the call line.
# If --help is pressent in the arg (as key or value) then the help procedure will be enacted

Main = perams('Configeration for the general functioning of the script',
MODEL_NAME=(str,'HISENBERG SPIN CHAIN','Arbtray Name'),
OUTPUT = (Path,'output/<date time now>','Directory for output files to be writen too, relative path okay'),
DRYRUN = (bool,False,'If True, Arguments will be passed, Hamiltonion will be formed, and program will exit'),
WRITE_STATES =  (bool, True,'If True, Argument, States will be writen to the output director useing python pickel' ),
EXPLICIT_H = (bool, False,'If True, The numpy hamiltonions to be logged, aafter appropreate pretification'),
TESTS = (list,['all'],'Specification of physical tests to be run. If contains "all" then all avalibel tests will be run except for the onese listed along side "all"'),
MIRROR_APPEND = (bool, False, 'If True, Perform the mirror append transformation; Defult is False because I dont think this is a good stratage'),
)

Hyper = perams('Hyper peramiters for the simulation',
HAM = (str, '1','Which Hamiltonan to be used'),
LEVELS = (int,2,'Maximum Levels per mode, 2 corresponds to Fermion'),
CHAINLENGTH = (int,2,'Number of  cites in the spin chain, two modes per cite'),
U_METHOD = (str,'2','Which method is used to produce propergator for each time'),
SUBSPACE = (bool,True,'If True, The Initial state will be profield for which number and magnitisation subspace it belongs too, and only that subspace will be simulated'),
TIME_LEN = (float,5,'Length of time to be simulated'),
TIME_STEP = (float,0.01,'Temporal Resilution'),
)

Phys = perams('Tuabel,Physical Constants',
J = (float, 1,'Tunnelling Strength for Hubard model'),
U = (float, 1,'Oncite Interaction Strength for Hubard model'),
EPSILON = (dict, [],'Oncite potential for Hubard model, Dictionary pairing number with epsilon, defult epsilon is zero'),
Jxyz = (list,[1,1,1],'Directional Spin Coupeling for Hiesenberg model'),
)

Init = perams('Specification for the initilisation',
INIT_UPS = (list,[0],'Which cites to initiat in the Up configeration'),
REMOVE = (list,[],'which modes to remove all '),
)

FFT = perams('Specification for the Furia Transform',
Apply_FFT = (bool,True,'If true, FFT will be applyed'),
FFT_TEARMS = (int,1000,'If > 0, the number of furia components considered'),
WRITE_FT = (bool, True, 'If true, the FFT will be saved to the output directory useing python pickel'),
)

Main_Plot = perams('Specifications for ploting rughteen',
LOAD = (bool,True,'If true data will be loaded form file'),
LOAD_PATH = (Path,Path('/dev/null'),'Path of data files'),
)

def Plot_Spec(string_or_tuple):
    if type(string_or_tuple) == str:
        string = string_or_tuple
        lib,*rest = string.split() # first word is lib, rect is the spec handeled latter
        rest = tuple(section.strip() for section in ' '.join(rest).split('|')) #  the spec are commands seperated by a pipe
    elif type(string_or_tuple) == tuple:
        lib,*rest = string_or_tuple
    else:
        raise Exception 

    libs = {'mpl','alt','no'}
    assert lib in libs, f'First word indicates ploting library, avalable {libs}'
    return (lib,*rest)

Ploting = perams('Configeration for specificaly for the plotting roteens',
PLOT = (Plot_Spec,'mpl freq_range 1','Which ploting ruteen to be selected',NO_EVAL),
SHOW_PLOT = (bool,True,'Wether the plot should be displayed or simply writen to Output directory'),
WRITE_PLOT = (bool,True,'Wether to right the plot'),
PROSSESS = (bool,True,'Wether to prossess data befor pltting'),
PLOT_DIR = (Path, '.' ,'Directory plots will be writen too'),
)
