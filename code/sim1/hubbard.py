#!/bin/env python3
from loging import *
eval(log_env_str) # Gives the log file accsess to the global scope of this file

OUTPUT = f'output/{datetime.now()}'
if 'OUTPUT' in sys.argv:
    i = sys.argv.index('OUTPUT') + 1
    OUTPUT = sys.argv[i]
OUTPUT = Path(OUTPUT)
if not OUTPUT.exists():
    OUTPUT.mkdir()
PERAMS = OUTPUT('perams.pickle')

log_config(OUTPUT = OUTPUT, LOGFILE = OUTPUT('log.txt'))
log('started')

from scipy import linalg
import numpy as np
import simal as sl
import simulator as sim
import pandas as pd
from pyrsistent import freeze
import peramiters as peram

peram.Defult_OUTPUT = OUTPUT

Config_Main = tee('Main.pickle',peram.Main().set('OUTPUT',OUTPUT))
Config_Plot = tee('Ploting.pickle',peram.Ploting())
Config_HyperPerams = tee('Hyper.pickle',peram.Hyper())
Config_PhysicsPerams = tee('Physics.pickle',peram.Phys())
Config_Initilisation = tee('init.pickle',peram.Init())
Config_FFT = tee('fft.pickle',peram.FFT())


if Config_Main == None:
    # this implys --help was pressent in call line
    sys.exit()


sl.LEVELS = Config_HyperPerams.LEVELS

INITIAL = sim.remove(sim.Down_But(Config_Initilisation.INIT_UPS,Config_HyperPerams.CHAINLENGTH),Config_Initilisation.REMOVE)

basis = basis_compliment = pbasis = sl.brange(Config_HyperPerams.CHAINLENGTH)

if Config_HyperPerams.SUBSPACE: 
    if Config_HyperPerams.HAM == 'HIS':
        basis = one_atom_per_cite(basis)
        pbasis = [tuple({(0,1):'+',(1,0):'-'}[cite]
            for cite in zip(b[::2],b[1::2]))
            for b in basis]
    else: 
        basis = sl.appropreate_subspace(basis,INITIAL)

basis_compliment = list(set(basis_compliment) - set(basis))

log_keep('basis',basis)
cast = sl.mkcast(basis)

log('Hillbert space dimention',len(basis))

log('configuerd')

if Config_Main.DRYRUN:
    log('exiting due to dry run flag')
    sys.exit()

with computing('Hamiltonan'):
    H_sim = sim.provided_hamiltonions[Config_HyperPerams.HAM](\
            CHAINLENGTH = Config_HyperPerams.CHAINLENGTH,
            **Config_PhysicsPerams
            #EPSILON = Config.EPSILON,
            #J=Config.J,U=Config.U,Jxys=Config.Jxyz
            )
    log(H_sim)
    H = cast(H_sim)

if Config_Main.EXPLICIT_H:
    print_matrix(H,basis=pbasis,print=log)

computing('lattice initial state')
state_initial = np.zeros(len(basis))
state_initial[basis.index(INITIAL)] = 1
log_keep('init',state_initial)


def Compute_Time_Domain(
        Config_HyperPerams = Config_HyperPerams,
        Config_PhysicsPerams = Config_PhysicsPerams,
        ):
    with computing('unitarys and states'):
        time = np.arange(0,Config_HyperPerams.TIME_LEN,Config_HyperPerams.TIME_STEP)
        prog = log_bar('time stpes',len(time))
        propergator = sim.provided_unitary_methods[Config_HyperPerams.U_METHOD](H,time)

        Data_Generators = [sim.Compute_States(state_initial,propergator)]

        data_accumulated = {'Model':[],'Time':[],'Cite':[],'Obs':[],'Value':[]}
        def data_accumulate(mode_Id,time,cite,observation,value):
            data_accumulated['Model'].append(model_id)
            data_accumulated['Time'].append(time)
            data_accumulated['Cite'].append(cite)
            data_accumulated['Obs'].append(observation)
            data_accumulated['Value'].append(value)


        Op_num = [ cast(sl.n(sl.mode(cn,'UP')) + sl.n(sl.mode(cn,'DOWN')))
                       for cn in range(Config_HyperPerams.CHAINLENGTH) ]

        Op_mag = [ cast(sl.n(sl.mode(cn,'UP')) - sl.n(sl.mode(cn,'DOWN')))
                       for cn in range(Config_HyperPerams.CHAINLENGTH) ]


        for model_id,Generator in enumerate(Data_Generators):
            for t,state in enumerate(Generator):
                for i in range(Config_HyperPerams.CHAINLENGTH):

                    exp_n = sl.expectation(Op_num[i],state)
                    exp_m = sl.expectation(Op_mag[i],state)

                    data_accumulate(model_id,time[t],i,'Num',exp_n)
                    data_accumulate(model_id,time[t],i,'Mag',exp_m)
    return pd.DataFrame(data_accumulated) # dataframe time domain

df_td = Compute_Time_Domain()
    

def Compute_Freq_Domain(df_td = df_td,
        Config_HyperPerams = Config_HyperPerams):
    with computing('fft'):
        _fds = [] # freq domains
        for m in set(df_td.Model):
            for i in set(df_td.Cite):
                for o in set(df_td.Obs):
                    _fd = df_td.loc[ (df_td.Model == m)
                                     & (df_td.Cite == i )
                                     & (df_td.Obs == o  )].sort_values('Time').copy()

                    # datafram frequancy domain
                    index_of_time = list(_fd).index('Time')

                    time = _fd.Time
                    del _fd['Time']
                    _fd.insert(index_of_time,'Freq',
                     np.fft.fftfreq(len(time),
                                    d = Config_HyperPerams.TIME_STEP))

                    value = _fd.Value
                    del _fd['Value']
                    _fd['Value'] = np.fft.fft(value)
                    
                    ang = _fd.copy()
                    abs = _fd.copy()
                    
                    ang.Value = ang.Value.transform(np.angle)
                    abs.Value = abs.Value.transform(np.abs)
            
                    ang['Part'] = 'Phase'
                    abs['Part'] = 'Magnitude'
                    
                    _fd = pd.concat([ang,abs])
                    _fds.append(_fd)

    df_fd = pd.concat(_fds)
    return df_fd

if Config_FFT.Apply_FFT:
    df_fd = Compute_Freq_Domain()
    if Config_FFT.WRITE_FT:
        log('writing frequancy domain')
        log_keep('frequancy_domain.pkl',df_fd)

if Config_Main.WRITE_STATES:
    log('writing time domain')
    log_keep('time_domain.pkl',df_td)


log('caption')
log(f'hubbard model of {Config_HyperPerams.CHAINLENGTH} cites, J = {Config_PhysicsPerams.J}, U = {Config_PhysicsPerams.U}.')
log_comps()


if Config_Plot.PLOT[0] != 'no':
    log('calling to plot')
    globals()['plotting'] = import_local('plotting')
    plotting.main(
            data=freeze(dict(Time=df_td,Freq=df_fd)),
            log = log,
            **Config_Plot.set('LOAD_PATH',Config_Main.OUTPUT),
            **Config_HyperPerams,**Config_PhysicsPerams)

log('done')
