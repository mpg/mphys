import simulator as sim
import numpy as np
from helpers import Acuumulator, trunkate
import matplotlib.pyplot as plt

scope = None

def limmet(time,limet):
    return time[time < limet]

def Args(*pargs,**kwargs):
    return (pargs,kwargs)

def apply(foo,arg):
    return foo(*arg[0],**arg[1])

def HUB(
        alpha = 0.1,
        CHAINLENGTH = 3,
        time = np.arange(0,200,0.01),
        epsilons = None,
        ):

    epsilons = epsilons or CHAINLENGTH*[0] 

    epsilons = {i:epsilons[i] for i in range(len(epsilons))}

    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial)
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    H_sim = (1/(2*alpha**2))*sim.provided_hamiltonions['2'](1,alpha,epsilons,CHAINLENGTH)

    H = cast(H_sim)

    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)

    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    return time,Ns.arraysT(),Zs.arraysT()

def HIS(
        alpha = 0.1,
        CHAINLENGTH = 3,
        time = np.arange(0,200,0.01),
        epsilons = None,
        ):

    epsilons = epsilons or CHAINLENGTH*[0] 

    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.one_atom_per_cite(sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial))
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    H_sim = (1/(2*alpha**2))*sim.provided_hamiltonions['aniso'](CHAINLENGTH,U=1,alpha=alpha,eps=epsilons)

    H = cast(H_sim)

    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)
    
    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    return time,Ns.arraysT(),Zs.arraysT()

def HHNZ(L,alpha,eps,ref_time = np.arange(0,200,0.1)):
    epsilons = [ eps if i == 0 else 0 for i in range(L) ]

    hub_time, hub_Ns, hub_Zs = HUB( alpha = alpha, CHAINLENGTH = L, time = ref_time, epsilons = epsilons)
    his_time, his_Ns, his_Zs = HIS( alpha = alpha, CHAINLENGTH = L, time = ref_time, epsilons = epsilons)

    return Args( (hub_time,his_time), (hub_Ns,his_Ns), (hub_Zs,his_Zs), L = L, alpha = alpha, eps = eps, domain = 'time')

def Curves(time,lines):
    return list(zip(len(lines)*[time],lines,range(len(lines))))

def N(args):
    pos_args, kw_args = args
    hub_time, his_time = pos_args[0]
    hub_Ns, his_Ns = pos_args[1]

    hub_curvs = Curves(hub_time, hub_Ns)
    his_curvs = Curves(his_time, his_Ns)

    kws = kw_args.copy()
    kws.update(dict( filter = 'N'))
    return ((hub_curvs, his_curvs), kws)

def Z(args):
    pos_args, kw_args = args
    hub_time, his_time = pos_args[0]
    hub_Zs, his_Zs = pos_args[2]
    
    hub_curvs = Curves(hub_time, hub_Zs)
    his_curvs = Curves(his_time, his_Zs)

    kws = kw_args.copy()
    kws.update(dict( filter = 'Z'))
    return ((hub_curvs, his_curvs), kws)

def NZ(args):
    pos_args, kw_args = args
    hub_time, his_time = pos_args[0]
    hub_Ns, his_Ns = pos_args[1]
    hub_Zs, his_Zs = pos_args[2]
    
    hub_curvs = Curves(hub_time, hub_Zs) + Curves(hub_time, hub_Ns)
    his_curvs = Curves(his_time, his_Zs) + Curves(his_time, his_Ns)

    kws = kw_args.copy()
    kws.update(dict( filter = 'NZ'))
    return ((hub_curvs, his_curvs), kws)

def DIFF(args):
    curves,_kws = args
    kws = _kws.copy()
    hub,his = curves
    diff_curves = []
    for i in range(len(hub)):
        hub_time,hub_trace,hub_site = hub[i]
        his_time,his_trace,his_site = his[i]
        assert np.all(his_time == hub_time)
        assert hub_site == his_site
        diff_trace = his_trace - hub_trace
        diff_curves.append( (hub_time, diff_trace, hub_site) )

    kws.update({'choice':'diff'})
    return (diff_curves, kws)

def STAR(args):
    curves,_kws = args
    kws = _kws.copy()
    both_curves = curves[0] + curves[1]
    kws.update({'choice':'star'})
    return (both_curves,kws)

def CHUB(args):
   curves,_kws = args
   kws = _kws.copy()
   both_curves = curves[0]
   kws.update({'choice':'hub'})
   return (both_curves,kws)

def CHIS(args):
   curves,_kws = args
   kws = _kws.copy()
   both_curves = curves[1] 
   kws.update({'choice':'his'})
   return (both_curves,kws)
 

def _fft(time,*series):
    time,*series = trunkate(time,*series)
    freq = np.fft.fftfreq(len(time),time[1]) 
    range = freq > 0
    freq = freq[range]
    tran = [np.abs(np.fft.fft(s)[range]) for s in series]
    return (freq,*tran)


def FFT(args):
    curves,_kws = args
    kws = _kws.copy()
    ffts = []
    for curve in curves:
        time,amp,site = curve
        freq,mag = _fft(time,amp)
        ffts.append((freq,mag,site))
    kws.update({'domain':'freq'})
    return (ffts,kws)

def _peaks(freq,mag,threshold = 1/10):
    max_mag = max(mag)
    dif = np.sign(np.diff(mag)+1)
    diff = np.diff(dif)
    d = (np.abs(diff) > 1.5) & ( mag[1:-1] > max_mag*threshold )
    return freq[1:-1][d]

def PEAKS(args, threshold = 1/10):
    curves,_kws = args
    kws = _kws.copy()
    kws.update({'threshold': threshold})
    peaks = []
    for freq,mag,site in curves:
        meta = kws.copy()
        meta.update({'site': site})
        peaks.append((_peaks(freq,mag,threshold),meta))
        del meta
    return (peaks,kws)

def _cite_one_peaks(peaksss):
    returns = []
    for peakss in peaksss:
        for peaks,kws in peakss[0]:
            if kws['site'] != 0:
                continue
            returns.append((peaks,peakss[1]))
    return returns


def PEAK_STATS(peaks_hub,peaks_his):
    hubs = _cite_one_peaks(peaks_hub)
    hiss = _cite_one_peaks(peaks_his)
    for hub,his in zip(hubs,hiss):
        _stats(hub[0],his[0])
    global scope
    scope = locals()



def Plot_Peaks(index,peakss,fig_args = {}):
    cmap = {'his':'blue','hub':'red'}
    fig,ax = plt.subplots(**fig_args)
    ax.grid()
    index_name = {'alpha': r'$\alpha$', 'eps': r'$\epsilon_0$'}[index]
    ax.set_xlabel(index_name)
    ax.set_ylabel('Simulation Frequency')
    for args in peakss:
        for peaks in args[0]:
            if peaks[1]['site'] != 0:
                continue
            for peak in peaks[0]:
                plt.scatter(peaks[1][index],peak,c = cmap[peaks[1]['choice']])

    return fig

'Magnetisation Difference'

def Plot_TDFD(args,max_time = 5,max_freq=1,fig_args={}):
    TD,_kws = args
    FD,_ = FFT(args)
    kws = _kws.copy()
    fig,axs = plt.subplots(kws['L'],2,sharex='col',**fig_args)
    axsl,axsr = axs.T
    [ ax.grid() for ax in axsl ]
    [ ax.grid() for ax in axsr ]
    axsl[-1].set_xlabel('Simulation Time')
    axsr[-1].set_xlabel('Simulation Frequency')
    for time,amp,site in TD:
        axsl[site].plot(time[time< max_time],amp[time<max_time])
    for freq,mag,site in FD:
        axsr[site].set_yticklabels([])
        axsr[site].plot(freq[freq < max_freq],mag[freq < max_freq])

    return fig

def Plot_Sites(args,fig_args = {} ):
    curves,_kws = args
    kws = _kws.copy()
    fig,axs = plt.subplots(kws['L'],sharex=True,**fig_args)
    index_max = {'time': 25, 'freq': 3}[kws['domain']]
    index_name = {'time':'Simulation Time','freq':'Frequency'}[kws['domain']]
    fig.text(0.5, 0.04, index_name, ha='center')
    fig.text(0.04, 0.5, 'common Y', va='center', rotation='vertical')
    [ ax.grid() for ax in axs ]
    for index,value,site in curves:
        index_range = index < index_max
        axs[site].plot(index[index_range],value[index_range])
    return fig


#for L in [2,3]:
#    for eps in [0,0.1,0.2,0.3]:
#        alphas = [0.01,0.02,0.03,0.04]
if __name__ == '__main__' and False:
    SAVE = True
    DEL = True
    for L in [3]:
        for eps in [0]:
            alphas = [0.01]
            Zpeaks_hub = []
            Zpeaks_his = []
            for alpha in alphas:
                ARGS = HHNZ(L,alpha,eps,ref_time=np.arange(0,500,0.01)) 
                
                meta = args[1]
                peram_name = 'L{L}A{alpha}E{eps}'.format(**meta)
                Plot_TDFD(STAR(NZ(ARGS))).savefig(f'results/moap/{peram_name}_TDFD.pdf')

                fig = Plot_Sites( STAR(Z(ARGS)) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_STAR_Z_TD.pdf')
                if DEL: del fig

                fig = Plot_Sites( DIFF(Z(ARGS)) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_DIFF_Z_TD.pdf')
                if DEL: del fig

                fig = Plot_Sites( STAR(N(ARGS)) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_STAR_N_TD.pdf')
                if DEL: del fig

                fig = Plot_Sites( DIFF(N(ARGS)) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_DIFF_N_TD.pdf')
                if DEL: del fig

                fig = Plot_Sites( FFT(STAR(Z(ARGS))) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_STAR_Z_FD.pdf')
                if DEL: del fig

                fig = Plot_Sites( FFT(DIFF(Z(ARGS))) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_DIFF_Z_FD.pdf')
                if DEL: del fig

                fig = Plot_Sites( FFT(STAR(N(ARGS))) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_STAR_N_FD.pdf')
                if DEL: del fig

                fig = Plot_Sites( FFT(DIFF(N(ARGS))) )
                if SAVE: fig.savefig(f'results/moap/{peram_name}_DIFF_N_FD.pdf')
                if DEL: del fig


                Zpeaks_hub.append( PEAKS(FFT(CHUB(Z(ARGS)))) )
                Zpeaks_his.append( PEAKS(FFT(CHIS(Z(ARGS)))) )

            Plot_Peaks('alpha',Zpeaks_hub + Zpeaks_his).savefig(f'results/moap/{peram_name}_alpha_peak_scatter.pdf')

    #plt.show().
