#!/bin/env python

import simulator as sim
import numpy as np
from helpers import Acuumulator

scope = None


def comparision_data(
        J = 1,
        alpha = 100, # alpha = U / J
        Delta = 1,
        Jeff = 1,
        time = np.arange(0,200,0.1),
        CHAINLENGTH = 3,
        epsilons = {},
        ):

    initial_ocupied = sim.Down_But([0],CHAINLENGTH)
    initial_deficet = sim.remove(initial_ocupied,[1])

    basis_full = sim.brange(CHAINLENGTH)
    basis_ocupied = sim.appropreate_subspace(basis_full,initial_ocupied)
    basis_deficet = sim.appropreate_subspace(basis_full,initial_deficet)

    cast_full = sim.mkcast(basis_full)
    cast_ocupied = sim.mkcast(basis_ocupied)
    cast_deficet = sim.mkcast(basis_deficet)

    Initial_ocupied = cast_ocupied(initial_ocupied)
    Initial_deficet = cast_deficet(initial_deficet)

    H_sim = sim.provided_hamiltonions['2'](alpha,1,epsilons,CHAINLENGTH)
    H_ocupied = cast_ocupied(H_sim)
    H_deficet = cast_deficet(H_sim)


    print('printing fully ocupied')
    sim.print_matrix(H_ocupied,basis_ocupied)

    print('printing with defect')
    sim.print_matrix(H_deficet,basis_deficet)

    print('defining observer operators')

    Op_Ns_full = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast_full)
    Op_Zs_full = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast_full)

    Op_Ns_ocupied = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast_ocupied)
    Op_Zs_ocupied = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast_ocupied)
    
    Op_Ns_deficet = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast_deficet)
    Op_Zs_deficet = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast_deficet)
    
    U = sim.provided_unitary_methods['itteration']

    Sim_ocupied = sim.Compute_States(Initial_ocupied,U(H_ocupied,time))
    Sim_deficet = sim.Compute_States(Initial_deficet,U(H_deficet,time))
    
    data_ocupied = Acuumulator()
    data_deficet = Acuumulator()
    for t,State_ocupied,State_deficet in zip(time,Sim_ocupied,Sim_deficet):
        if t % 5 == 0:
            print(f'-> {t:0.2f}',end='\r')

        data_ocupied(
                Ns = [ N(State_ocupied) for N in Op_Ns_ocupied ],
                Ms = [ M(State_deficet) for M in Op_Zs_ocupied ],
                )

        data_deficet(
                Ns = [ N(State_ocupied) for N in Op_Ns_deficet ],
                Ms = [ M(State_deficet) for M in Op_Zs_deficet ],
                )

    global scope
    scope = locals()

    return time,data_ocupied.arraysT(),data_deficet.arraysT()


def phft(time,td_hub,td_his):
    'posative half furia transform'

    freq = np.fft.fftfreq( n = len(time), d = time[1] - time[0] )
    frange = freq > 0
    freq = freq[frange]

    def fft(array):
        return abs(np.fft.fft(array)[frange])

    fd_hub = Acuumulator()
    fd_his = Acuumulator()
    for cite in range(len(td_hub[0])):
        fd_hub( Ns = fft( td_hub[0][cite] ),
                Ms = fft( td_hub[1][cite] ))

        fd_his( Ns = fft( td_his[0][cite] ),
                Ms = fft( td_his[1][cite] ))

    return freq,fd_hub.arrays(),fd_his.arrays()

def align_factor(freq,M_hub,M_his):
    f_peaks_hub = [ freq[ M == M.max() ][0] for M in M_hub ]
    f_peaks_his = [ freq[ M == M.max() ][0] for M in M_his ]

    f_peak_hub = f_peaks_hub[0]
    f_peak_his = f_peaks_his[0]

    assert np.all(f_peaks_hub == f_peaks_hub),str(f_peaks_hub)
    assert np.all(f_peaks_his == f_peaks_his),str(f_peaks_his)

    print(f_peaks_hub,f_peaks_his)

    return f_peak_his/f_peak_hub

def factor(**config):
    freq, phft_hub, phft_his = phft(*comparision_data(**config))
    factor = align_factor(freq,phft_hub[1],phft_his[1])

    return factor,freq[0]


import matplotlib.pyplot as plt
def plot(index1,index2,data_hub,data_his,range1,range2):
    index1 = index1[range1]
    index2 = index2[range2]

    Ns_hub, Ms_hub = data_hub
    Ns_his, Ms_his = data_his

    CHAINLENGTH = len(Ns_hub)

    N_tot_hub = np.sum(Ns_hub,axis=0)
    M_tot_hub = np.sum(Ms_hub,axis=0)

    N_tot_his = np.sum(Ns_his,axis=0)
    M_tot_his = np.sum(Ms_his,axis=0)

    fig,axs = plt.subplots(CHAINLENGTH+1,2,sharex=False)
    axsl,axsr = axs.T

    for i,ax in enumerate(axsl):
        ax.grid()
        if i == 0:
            ax.set_title('One atom per cite')
        if i == CHAINLENGTH:
            ax.plot(index1,N_tot_hub[range1])
            ax.plot(index1,M_tot_hub[range1])
            continue

        ax.plot(index1,Ns_hub[i][range1])
        ax.plot(index1,Ms_hub[i][range1])

    for i,ax in enumerate(axsr):
        ax.grid()
        if i == 0:
            ax.set_title('Defect second cite')
        if i == CHAINLENGTH:
            ax.plot(index2,N_tot_his[range2])
            ax.plot(index2,M_tot_his[range2])
            continue

        ax.plot(index2,Ns_his[i][range2])
        ax.plot(index2,Ms_his[i][range2])

if __name__ == '__main__':

    time, data_hub, data_his = comparision_data()
    freq, phft_hub, phft_his = phft(time, data_hub, data_his)
    factor = align_factor(freq,phft_hub[1],phft_his[1])

    range1 = time < 25
    range2 = (factor*time) < 25

    all_freq = freq == freq
    
    plot(freq, freq, phft_hub, phft_his, all_freq, all_freq)
    plt.tight_layout()
    #plt.savefig('results/defect/new2.pdf')
    
    plot(time, factor*time, data_hub, data_his, range1, range2)
    plt.tight_layout()
    #plt.savefig('results/defect/new1.pdf')

    plt.show()
