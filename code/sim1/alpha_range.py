import simulator as sim
import numpy as np
from helpers import Acuumulator, trunkate
import matplotlib.pyplot as plt

scope = None

def compute(
        U = 1,
        alpha = 0.1,
        Delta = 0,
        Jeff = 1,
        CHAINLENGTH = 3,
        ):

    time = np.arange(0,2000,0.01)

    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial)
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    epsilons = {n:((-0.5)**n)*Delta for n in range(CHAINLENGTH)}
    H_sim = (1/(alpha**2*U))*sim.provided_hamiltonions['2'](U,alpha*U,epsilons,CHAINLENGTH)

    H = cast(H_sim)

    print(alpha)
    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)

    
    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    global scope
    scope = locals()
    return time,Ns.arraysT(),Zs.arraysT()

def fft(time,*series):
    freq = np.fft.fftfreq(len(time),time[1]) 
    range = freq > 0
    freq = freq[range]
    tran = [np.abs(np.fft.fft(s)[range]) for s in series]
    return (freq,*tran)


one_over_alphas = [2,10,15,20]
fig,axs = plt.subplots(len(one_over_alphas),2)
fig.tight_layout()
axsl,axsr = axs.T
for i,a in enumerate(one_over_alphas):
    print(i,a)
    time,Ns,Zs = compute(alpha=1/a)
    N,Z = Ns[0],Zs[0]
    freq, fd_N, fd_Z = fft(time,N,Z)

    time_range = time < 15
    time = time[time_range]

    freq_range = freq < 5
    freq = freq[freq_range]


    axsl[i].grid()
    axsr[i].grid()

    axsl[i].plot(*trunkate(time,N),*trunkate(time,Z))
    axsr[i].plot(*trunkate(freq,fd_N),*trunkate(freq,fd_Z))
    axsl[i].set_ylabel(r'$\alpha = 1/{}$'.format(a))


plt.savefig('results/alpha/new.pdf')
plt.show()
