#!/bin/env python
import simulator as sim
import numpy as np
import sys

args = {k:a for k,a in zip(sys.argv[1::2],sys.argv[2::2])}

L = int(args.get('L') or 3)
J = float(args.get('J') or 0.1)
U = float(args.get('U') or 1)
D = float(args.get('Delta') or 0)

epsilons = {i:(-1)**i * D/2 for i in range(L)}

H_sim = sim.provided_hamiltonions['2'](U,J,epsilons,CHAINLENGTH = L)
basis = sim.brange(L)
cast = sim.mkcast(basis)
H = cast(H_sim).real
sim.print_matrix(H,basis)
np.save((args.get('-o') or 'output'), H,allow_pickle = True, fix_imports=True)

