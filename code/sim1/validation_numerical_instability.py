import simulator as sim
import numpy as np
from itertools import tee
import matplotlib.pyplot as plt

'''
This script is to test the numerical stability of the simulation by varying the time set.
'''

SHOW = True
SAVE = True

def every_n(G, n = 2):
    'helper function. Generator Transformer\
     Yields every nth item from an iterator.'
    i = -1 # yeaids first opperator
    for g in G:
        i = (i + 1)% n 
        if i == 0:
            yield g


def progressive(foo,Xs,**kwargs):
    'helper function. Geneerator Transformer\
    applys foo n times to the nth element of Xs.\
    kwargs are passed to foo.'
    for n,x in enumerate(Xs):
        for i in range(n):
            x = foo(x,**kwargs)
        yield x

def append_into(**kw):
    'syntactic swetener, appends the kew word values in to the keys,\
     as found in the global name space'
    for k in kw:
        globals()[k].append(kw[k])


def stats(array):
    'helper function to compute, average, standad deviaton and standad error'
    av = np.average(array)
    sd = np.sum((av - array)**2/len(array) )**0.5
    se = sd * len(array)**-0.5

    return av,sd,se

def mk_normal(mu,sig):
    'helper function that returns the gaussian function for a specifyed mu and sigma'
    return  lambda x: (1/(sig*np.sqrt(2*np.pi)))*np.exp(-0.5*((x - mu)/sig)**2)

def histogram(array):
    'helper function that calles numpys histogram feature in the way I want'
    hist,bin_edges = np.histogram(np.sort(products),bins=len(products)// 20 ,density=True)
    bin_centers = (bin_edges[1:] + bin_edges[:-1])/2
    return hist,bin_centers

CHAINLENGTH = 5
TIME_LEN = 1000
U,J = 1,1 # U oncite interaction, J tunneling strength
TIME_STEP = 0.1 # time step of courset step
Time_Ratio = 2 # ratio of time steps
Curves = 8

Initial_State_sim = sim.Down_But([0],CHAINLENGTH)
# Initial state in simbolic form.
# All cites are down exsept the cites specifyed in the first argument
# There are CHAINLENGTH cites

basis = sim.appropreate_subspace(sim.brange(CHAINLENGTH),Initial_State_sim)
# Only the subspace relevent to the initial states will be considered.
# If the returned basis is actually not subspace,
# the call to cast will raise an exception

cast = sim.mkcast(basis)
# cast takes statemaps to 2d numpy arrays and stest to 2d numpy arrays

H_sim = sim.provided_hamiltonions['standad hubbard'](U,J,{},CHAINLENGTH)
H_np = cast(H_sim)
Initial_State_np = cast(Initial_State_sim)

dt = lambda n: TIME_STEP*Time_Ratio**-n
# time step for the nth curve

times = [ np.arange(0,TIME_LEN,dt(n))
        for n in range(Curves)]

sampels = [len(time) for time in times]

Us = [ sim.provided_unitary_methods['itteration'](H_np,time)
        for time in times ]

Ss = list(progressive(every_n,
       [sim.Compute_States(Initial_State_np,U) 
          for U in Us ], n = Time_Ratio))

avs = []
sds = []
SEMs = []
hists = []
centers = []
normals = []
ns = []
As = []
print('\\begin{tabular}{ c | c c | c c }')
print('Samples & Product & SEM & Norm & SEM\\\\')
for i,S in enumerate(Ss):
    if i == 0:
        S = S0 =  np.array(list(S))
    products = []
    selfp = []
    selfa = []
    for s,s0 in zip(S,S0):
        products.append(abs(sim.inner(s0,s)))
        selfp.append(abs(sim.inner(s,s)))
        selfa.append(products[-1]/selfp[-1])

        

    products = np.array(products)

    av,sd,sem = stats(products)
    fitline = mk_normal(av,sd) # normal distribution
    self_p_av, self_p_sd, self_p_sem = stats(selfp)
    self_a_av, self_a_sd, self_a_sem = stats(selfa)

    hist,bin_centers = histogram(products)

    print(f'{sampels[i]} & {1-av:.3E}  & {sem:.3E}  & {1-self_p_av:.3E} & {self_p_sem:.3E}\\\\')
    append_into(# Syntactic shuger. x = y -> x.append(y)
            avs = av,
            sds = sd,
            SEMs = sem,
            normals = fitline,
            hists = hist,
            centers = bin_centers,
            As = selfa,
            ns = selfp,
            )

print('\\end{tabular}')

Sdirect = sim.Compute_States(  Initial_State_np,
                               sim.provided_unitary_methods['direct'](H_np,times[0]) ) 

A = sim.Compute_States(  Initial_State_np,
                               sim.provided_unitary_methods['direct'](H_np,times[0]) ) 

B = sim.Compute_States(  Initial_State_np,
                               sim.provided_unitary_methods['itteration'](H_np,times[0]) ) 


product_direct = [] # the product of the states with the direct U-method
product_comper = [] # comparison of states between teh direct and iteration U-methods
for t,direct,iteration in zip(times[0],Sdirect,S0):
    print(f'{t:.1f}',end='\r')
    append_into(
            product_direct = abs(sim.inner(direct,direct)),
            product_comper = abs(sim.inner(direct,iteration))
            )


hist_direct,bin_centers_direct = histogram(direct)
av_comper, sd_comper, err_comper = stats(product_comper)
av_direct, sd_direct, err_direct = stats(product_direct)
normal_direct = mk_normal(av_direct,sd_direct)

fig,axs = plt.subplots(2,figsize = plt.figaspect(0.5))
ax1,ax2 = axs

ax1.grid()
ax2.grid()

ax1.step(bin_centers_direct,hist_direct)
ax1.plot(bin_centers_direct,normal_direct(bin_centers_direct))
ax1.set_xlim(bin_centers_direct[0],bin_centers_direct[-1])
ax1.set_title(r'$\mu = 1 + ({:.3E} \pm {:.3E} )$'.format(av_direct - 1,err_direct))
ax1.set_ylabel('Direct Method ')


center = centers[0]
hist = hists[0]
normal = normals[0]
av,err = avs[i],SEMs[0]
ax2.set_title(r'$\mu = 1 + ({:.3E} \pm {:.3E} )$'.format(av - 1,err))
ax2.set_ylabel('Iterative Method')

ax2.step(center,hist)
ax2.plot(center,normal(center))
ax2.set_xlim(center[0],center[-1])
fig.tight_layout()

if SAVE: fig.savefig('results/validation/nummerical_stability_0.pdf')
if SHOW: plt.show()

fig,ax = plt.subplots(figsize = plt.figaspect(0.5))
ax.errorbar(sampels,[1-av for av in avs],yerr=SEMs,fmt='o-')
ax.grid()
ax.set_xlabel('number or time steps')
ax.set_xscale('log')
ax.set_ylabel(r'1 - Average inner product with reference curve')
fig.tight_layout()
if SAVE: fig.savefig('results/validation/nummerical_stability_1.pdf')
if SHOW: plt.show()

Plots = 3
fig,axs = plt.subplots(min(Curves,Plots),figsize = plt.figaspect(0.5))
for j,ax in enumerate(axs):
    i = j * Curves//Plots
    center = centers[i]
    hist = hists[i]
    normal = normals[i]
    av,err = avs[i],SEMs[i]
    ax.set_title(r'$\mu = 1 + ({:.3E} \pm {:.3E} )$'.format(av - 1,err))
    ax.set_ylabel(r'{} samples'.format(sampels[i]))
    ax.set_xlabel('product')
    
    ax.step(center,hist)
    ax.plot(center,normal(center))
    ax.set_xlim(center[0],center[-1])

    ax.grid()

fig.tight_layout()
if SAVE: fig.savefig('results/validation/nummerical_stability_2.pdf')
if SHOW: plt.show()

fig,ax = plt.subplots(figsize = plt.figaspect(0.5))
ax.grid()
for i in range(len(hists)):
    center = centers[i]
    hist = hists[i]
    normal = normals[i]
    av,err = avs[i],SEMs[i]
    print(i,av,err)
    
    ax.step(center,hist)
    ax.plot(center,normal(center))
    ax.set_xlabel('product')
    ax.set_ylabel('density')
ax.legend()


if SAVE: fig.savefig('results/validation/nummerical_stability_3.pdf')
if SHOW: plt.show()
