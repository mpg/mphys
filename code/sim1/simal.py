import numpy as np
from pyrsistent import PRecord,PMap,field,freeze

def reduce(foo):
    def compute(xs):
        running,xs = xs[0],xs[1:]
        for x in xs:
            running = foo(running,x)
        return running
    return compute

# numpy operations are always biops even when there assosiative
kron = reduce(np.kron) # Kronicker Product of input list
matp = reduce(np.matmul) 
inner = lambda a,b,**s: np.sum(a.conj()*b,**s)
expectation = lambda O,v: inner(v,O@v).real
mag = lambda a: inner(a,a)

class Named_Functions:
    def __init__(self,name,f):
        self.name = name
        self.f = f
    def __repr__(self):
        return self.name
    def __call__(self,*args,**kwargs):
        return self.f(*args,**kwargs)

def Name(name):
    return lambda foo: Named_Functions(name,foo)

LEVELS = 2
def levels(n=None):
    if n != None:
        globals()['LEVELS'] = n
    return globals()['LEVELS'] 

def cart(Ls):
    if len(Ls) == 0:
        return None
    Ls = list(Ls)
    L = [(l,) for l in Ls.pop()]
    while len(Ls) != 0:
        tmp = []
        for l1 in Ls.pop():
            for l2 in L:
                tmp.append((l1,*l2))
        L = tmp
    return L

def brange(n):
    return cart(2*n*[range(LEVELS)])

spins = {'DOWN':0,'UP':1}
def mode(i,sigma):
    return 2*i + spins[sigma]

def tearm_prod(L):
    if len(L) == 1:
        return L[0]
    if len(L) == 0:
        return None
    L = list(L)
    S,M = L.pop(0)
    Names = f'{M}'
    while len(L) > 0:
        s,m = L.pop(0)
        S *= s
        _M = M
        def M(basis):
            s1,b1 = m(basis)
            s2,b2 = _M(b1)
            return (s1*s2,b2)
        #M = lambda basis: _M(m(basis))
        Names += f'{m}'
    return (S,Name(Names)(M))

class state_map(PRecord): # list of tuple of number and function
    name = field(initial='?',type=str)
    tearms = field(mandatory=True,
            initial = freeze([]),
            factory = freeze,
            invariant = lambda data: ((type(data) == type(freeze(list()))\
                        and all([type(d) == tuple for d in data])\
                        and all([callable(d[1]) for d in data])),
                        'tearms fied must be a list of a number and a function'))
    def empty(self):
        return len(self.tearms) == 0

    def __str__(self):
        return self.name
    __repr__ = __str__

    def __call__(self,basis):
        result = []
        for scaler,bmap in self.tearms:
            si,image = bmap(basis)
            if image != None:
                result.append((scaler*si,image))
        return freeze(result)

    def __add__(self,other):
        if type(other) in {int,float,complex}:
            other = other*One
        if other.empty():
            return self
        if self.empty():
            return other

        return self.set(name= f'{self} + {other}',
                        tearms = self.tearms.extend(other.tearms))

    def __sub__(self,other):
        return self + (-1)*other

    __radd__ = __add__
    __rsub__ = __sub__

    def __mul__(self,other):
        data = []
        if type(other) in {int,float,complex}:
            name = f'{other}*({self})'
            for s,bmap in self.tearms:
                data.append((other*s,bmap))

        elif type(other) == type(self):
            data = [tearm_prod(neo) for neo in cart([self.tearms,other.tearms])]
            sn = f'{self}' if len(self.tearms) == 1 else f'({self})'
            on = f'{other}' if len(other.tearms) == 1 else f'({other})'
            name = sn + on
                
        else:
            raise NotImplemented

        return self.set(name = name,
                        tearms = data)
    def __pow__(self,n):
        assert type(n) == int
        assert n >= 0
        name = self.name + f'^{n}'
        running = self
        while n > 1:
            running = running*self 
            n -= 1
        return running.set(name=name)


    __rmul__ = __mul__

    def print_tearms(self):
        list(map(print,self.tearms))

zero_map = state_map(name='0')
One = state_map(tearms = [(1,Name('[.]')(lambda b: (1,b) ))],name='I')

def adag(i):
    @Name(f'[{i}+]')
    def bmap(basis):
        if basis == None or basis[i] == LEVELS - 1 :
            return 1,None
        n = basis[i] 
        basis = list(basis)
        basis[i] = n + 1
        return ((n+1)**0.5,tuple(basis))
    return state_map(tearms = [(1,bmap)],name = 'a^\dag_{'+str(i)+'}')


def a(i):
    @Name(f'[{i}-]')
    def bmap(basis):
        if basis == None or basis[i] == 0:
            return 1,None
        n = basis[i] 
        basis = list(basis)
        basis[i] = n - 1
        return (n**0.5,tuple(basis))
    return state_map(tearms = [(1,bmap)],name='a_{'+str(i)+'}')

def n(i):
    return (adag(i)*a(i)).set(name = 'n_{'+str(i)+'}')

def Z(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (n(up) - n(do)).set(name=f'Z_{i}')

def X(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (adag(up)*a(do) + adag(do)*a(up)).set(name=f'X_{i}')

def Y(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (-1j*(adag(up)*a(do) - adag(do)*a(up))).set(name=f'Y_{i}')



def mkcast(basis):
    '''
    cast from the simbolic form defined in this file to
    a numpy form useable in the simulation mecheanary.
    This function dispatches on instance. 
    state_maps -> numpy 2d arrays
    states     -> numpy 1d arrays
    '''

    def cast_vector(state):
        vector = np.zeros(len(basis))
        vector[basis.index(state)] = 1
        return vector

    def cast_matrix(state_map):
        matrix = np.zeros((len(basis),len(basis)),dtype='complex128')
        for i,vector in enumerate(basis):
            for scailer,image in state_map(vector):
                matrix[basis.index(image),i] += scailer
        return matrix

    dispatch_tabel = {
            state_map: cast_matrix,
            tuple: cast_vector
            }

    def dispatch(state_or_state_map):
        for inst in dispatch_tabel:
            if isinstance(state_or_state_map,inst):
                return dispatch_tabel[inst](state_or_state_map)
        else:
            raise Exception('Unable to cast')

    return dispatch



def n_sig(b,sig):
    return sum(b[i] for i in range(len(b)) if (i+sig) % 2 == 0)

def _subspace(basis,test):
    return [b for b in basis if test(b)]

def nm_subspace(basis,n,m):
    return _subspace(basis,lambda b: n_sig(b,0) == n and n_sig(b,1) == m)

def N_subspace(basis,N):
    return _subspace(basis,lambda b: n_sig(b,0) + n_sig(b,1) == N)

def average_one_subspace(basis):
    return _subspace(basis,
            lambda b: all(map(
                lambda cite: sum(cite) ==1,
                zip(b[::2],b[1::2]))))


def appropreate_subspace(basis,b):
    return nm_subspace(basis,n_sig(b,0),n_sig(b,1))

def one_atom_per_cite(basis):
    return _subspace(basis,
        lambda b: all(sum(cite) == 1
            for cite in zip(b[::2],b[1::2])))

def print_matrix(matrix,basis=None,print=print):
    print('warrning','decimal information forgotten in table formating')
    if np.any(matrix.imag != 0) and np.any(matrix.real != 0):
        formater = lambda n: f'{n:5.0f}'
    else:
        formater = lambda n: f'{n.real + n.imag:2.0f}'
    if np.all(matrix.real == 0) and np.any(matrix.imag != 0):
        print('OVER ALL FACTOR of i')
    if np.any(matrix.real != 0) and np.all(matrix.imag == 0):
        print('compleatly real')


    for lin,b in zip(matrix,basis):
        for n in lin:
            c = formater(n)
            if n == 0:
                c = ' '*(len(c)-1) + '.'
            print(c,end=' ')
        print('| ',b if type(b) == str else ''.join(map(str,b)))

def commute(A,B,basis):
    return numpyefy_matrix(A*B  - B*A,basis) == 0

def tsum(*tearms):
    return sum(*tearms,zero_map)
