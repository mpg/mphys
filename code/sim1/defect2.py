import simulator as sim
import numpy as np
from helpers import Acuumulator, trunkate
import matplotlib.pyplot as plt

scope = None

def H1S(
        alpha = 0.1,
        CHAINLENGTH = 3,
        time = np.arange(0,200,0.01),
        epsilons = {},
        ):


    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial)
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    H_sim = (1/(2*alpha**2))*sim.provided_hamiltonions['2'](1,alpha,epsilons,CHAINLENGTH)

    H = cast(H_sim)

    print(alpha)
    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)

    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    return time,Ns.arraysT(),Zs.arraysT()

def H2(
        alpha = 0.1,
        CHAINLENGTH = 3,
        time = np.arange(0,200,0.01),
        ):

    initial = sim.Down_But([0],CHAINLENGTH)
    basis = sim.one_atom_per_cite(sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial))
    cast = sim.mkcast(basis)    

    Initial = cast(initial)

    Jeff = 2*alpha**2
    H_sim = (1/(2*alpha**2))*sim.provided_hamiltonions['hisenberge'](CHAINLENGTH,3*[Jeff])


    H = cast(H_sim)

    print(alpha)
    sim.print_matrix(H,basis)

    Op_Ns = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast)
    Op_Zs = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast)

    
    U = sim.provided_unitary_methods['itteration']
    Sim = sim.Compute_States(Initial, U(H,time))

    Ns = Acuumulator()
    Zs = Acuumulator()
    for state in Sim:
        Ns( **{str(i): N(state) for i,N in enumerate(Op_Ns) } )
        Zs( **{str(i): Z(state) for i,Z in enumerate(Op_Zs) } )

    return time,Ns.arraysT(),Zs.arraysT()

def fft(time,*series):
    time,*series = trunkate(time,*series)
    freq = np.fft.fftfreq(len(time),time[1]) 
    range = freq > 0
    freq = freq[range]
    tran = [np.abs(np.fft.fft(s)[range]) for s in series]
    return (freq,*tran)

def plot(*curlves,legend=True,xlabel=None):
    fig,axs = plt.subplots(len(curlves[0][0][1]),sharex=True)
    if xlabel:
        axs[-1].set_xlabel = xlabel
    [ax.grid() for ax in axs]
    lines = []
    labels = []
    for pos,kw in curlves:
        time,Zs = pos
        print(len(time))
        for i,ax in enumerate(axs):
            line, = ax.plot(*trunkate(time,Zs[i]),**kw)
            if i == 0:
                lines.append(line)
                labels.append( kw.get('label') or '' )
    if legend:
        fig.legend(lines,labels)
    return fig

def peak(freq,amp):
    max_amp = max(amp)
    dif = np.sign(np.diff(amp)+1)
    diff = np.diff(dif)
    d = (np.abs(diff) > 1.5) & ( amp[1:-1] > max_amp/10 )
    return freq[1:-1][d]

def limmet(time,limet):
    return time[time < limet]

def args(*pargs,**kwargs):
    return (pargs,kwargs)

def apply(foo,arg):
    return foo(*arg[0],**arg[1])


CHAINLENGTH = 3
ref_time = np.arange(0,2000,0.01)

def emergent_plot(a = 0.1, deltas = [0,0.1,0.2]):
    #resids = []
    #his_time, his_Ns, his_Zs = H2( a, CHAINLENGTH, time = ref_time )
    #his_freq,*his_fft = fft(his_time,*his_Zs)
    #his_peak = peak(his_freq,his_fft[0])
    #tuple(his_peaks.append( (a,f) ) for f in his_peak)

    #running = 0
    #for hub,his in zip(hub_peak,his_peak):
    #    running += (hub - his) **2
    #running = np.sqrt(np.sum(running))/3
    #resids.append((a,running))

    hub_peaks = []
    his_peaks = []
    curves = []

    for di,d in enumerate(deltas):
        print(d,di/len(deltas))
        hub_time, hub_Ns, hub_Zs = H1S( a, CHAINLENGTH,epsilons={0:d}, time = ref_time )

        curves.extend([
            args(limmet(hub_time,10), hub_Ns,label = r'Occ $\epsilon_0 = {}$'.format(d) ),
            args(limmet(hub_time,10), hub_Zs,label = r'Mag $\epsilon_0 = {}$'.format(d) ) 
            ])

        hub_freq,*hub_fft = fft(hub_time,*hub_Zs)
        hub_peaks.extend([(d,p) for p in peak(hub_freq,hub_fft[0])])


    plot(*curves,xlabel = 'simulation time' ).savefig(f'results/defect/ontopof_alpha{a}_time.pdf')

    #fig,ax = plt.subplots()
    #ax.grid()
    #ax.scatter(*zip(*hub_peaks))

#   plot(
#           (limmet(hub_freq,0.5), hub_fft),
#           (limmet(his_freq,0.5), his_fft),
#           )#.savefig(f'results/emergent/ontopof_alpha{a}_freq.pdf')


    global scope
    scope = locals()

emergent_plot(a = 0.2)
plt.show()
