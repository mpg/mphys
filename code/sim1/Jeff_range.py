from helpers import Acuumulator
from finding_Jeff import *
import numpy as np
import matplotlib.pyplot as plt

alpha = 15
CHAINLENGTH = 3
time = np.arange(0,300,0.3)

J_variation = Acuumulator()
for j in range(10):
    J = 0.1*j
    f,err = factor(J = J ,alpha = alpha, CHAINLENGTH=3, time = time)
    J_variation( J = J, factor = f, error = err)

L_variation =  Acuumulator()
for L in range(2,8):
    J_one = J = 0.5
    f,err = factor(J = J, alpha = alpha, CHAINLENGTH = L, time = time)
    L_variation( L = L, factor = f, error = err )



L_variation2 =  Acuumulator()
for L in range(2,8):
    J_two = J = 0.25
    f,err = factor(J = J, alpha = alpha, CHAINLENGTH = L, time = time)
    L_variation2( L = L, factor = f, error = err )



delta_variation = Acuumulator()
for delta in range(10):
    Delta = 0.5*delta
    J = 0.5
    f,err = factor(J = J, alpha = alpha, CHAINLENGTH = 3, time = time, Delta = Delta)
    delta_variation( Delta = Delta, factor = f, error = err )

plt.grid()
plt.errorbar(*J_variation.arrays(),fmt='.-')
plt.xlabel(J_variation.keys[0])
plt.ylabel(J_variation.keys[1])
plt.title(r'$\alpha = {}, L = {}$'.format(alpha,CHAINLENGTH))
plt.savefig('results/factor/0.pdf')
plt.show()


plt.grid()
plt.errorbar(*L_variation.arrays(), fmt = '.-')
plt.xlabel(L_variation.keys[0])
plt.ylabel(L_variation.keys[1])
plt.title(r'$\alpha = {}, J = {}$'.format(alpha,J_one))
plt.savefig('results/factor/1.pdf')
plt.show()


plt.grid()
plt.errorbar(*L_variation2.arrays(), fmt = '.-')
plt.xlabel(L_variation2.keys[0])
plt.ylabel(L_variation2.keys[1])
plt.title(r'$\alpha = {}, J = {}$'.format(alpha,J_two))
plt.savefig('results/factor/2.pdf')
plt.show()

plt.grid()
plt.errorbar(*delta_variation.arrays(), fmt = '.-')
plt.xlabel(delta_variation.keys[0])
plt.ylabel(delta_variation.keys[1])
plt.title(r'$\alpha = {}, J = {}$'.format(alpha,J))
plt.savefig('results/factor/3.pdf')
plt.show()


