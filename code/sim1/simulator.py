import numpy as np
from scipy import linalg
import simal as sl
from pyrsistent import PRecord,PMap,field,freeze

def reduce(foo):
    def compute(xs):
        running,xs = xs[0],xs[1:]
        for x in xs:
            running = foo(running,x)
        return running
    return compute

# numpy operations are always biops even when there assosiative
kron = reduce(np.kron) # Kronicker Product of input list
matp = reduce(np.matmul) 
inner = lambda a,b,**s: np.sum(a.conj()*b,**s)
expectation = lambda O,v: inner(v,O@v).real
mag = lambda a: inner(a,a)

class Named_Functions:
    def __init__(self,name,f):
        self.name = name
        self.f = f
    def __repr__(self):
        return self.name
    def __call__(self,*args,**kwargs):
        return self.f(*args,**kwargs)

def Name(name):
    return lambda foo: Named_Functions(name,foo)

LEVELS = 2
def levels(n=None):
    if n != None:
        globals()['LEVELS'] = n
    return globals()['LEVELS'] 

def cart(Ls):
    if len(Ls) == 0:
        return None
    Ls = list(Ls)
    L = [(l,) for l in Ls.pop()]
    while len(Ls) != 0:
        tmp = []
        for l1 in Ls.pop():
            for l2 in L:
                tmp.append((l1,*l2))
        L = tmp
    return L

def brange(n):
    return [State(b) for b in cart(2*n*[range(LEVELS)])]

spins = {'DOWN':0,'UP':1}
def mode(i,sigma):
    return 2*i + spins[sigma]

def tearm_prod(L):
    if len(L) == 1:
        return L[0]
    if len(L) == 0:
        return None
    L = list(L)
    S,M = L.pop(0)
    Names = f'{M}'
    while len(L) > 0:
        s,m = L.pop(0)
        S *= s
        _M = M
        def M(basis):
            s1,b1 = m(basis)
            s2,b2 = _M(b1)
            return (s1*s2,b2)
        #M = lambda basis: _M(m(basis))
        Names += f'{m}'
    return (S,Name(Names)(M))


class State(tuple):
    _type = 'state'
    _cite_rep_dict = {
            (0,1): '↑',
            (1,0): '↓',
            (1,1): '⇑',
            (0,0): '⇓'}

    def __repr__(self):
        if len(self) % 2 != 0:
            return ''.join(map(str,self))
        return ''.join(self._cite_rep_dict[cite] for cite in zip(self[::2],self[1::2]))

    def cites(self):
        return tuple(State(cite) for cite in zip(self[::2],self[1::2]))

    def modes(self):
        return tuple(self)

    def numbers(self):
        return tuple(sum(cite[sig] for cite in self.cites())
                for sig in (0,1))


    def number_do(self):
        return self.numbers()[0]

    def number_up(self):
        return self.numbers()[1]

    def number_total(self):
        return sum(self.numbers())


class State_map(PRecord): # list of tuple of number and function
    _type = 'state map'
    name = field(initial='?',type=str)
    tearms = field(mandatory=True,
            initial = freeze([]),
            factory = freeze,
            invariant = lambda data: ((type(data) == type(freeze(list()))\
                        and all([isinstance(d,tuple) for d in data])\
                        and all([callable(d[1]) for d in data])),
                        'tearms fied must be a list of a number and a function'))
    def empty(self):
        return len(self.tearms) == 0

    def __str__(self):
        return self.name
    __repr__ = __str__

    def __call__(self,basis):
        scailers = {}
        for scaler,bmap in self.tearms:
            si,image = bmap(basis)
            if image != None:
                if image in scailers:
                    scailers[image] += scaler*si
                else:
                    scailers[image] = scaler*si
        return freeze([(scailers[image],image) for image in scailers])

    def __add__(self,other):
        if type(other) in {int,float,complex}:
            other = other*One
        if other.empty():
            return self
        if self.empty():
            return other

        return self.set(name= f'{self} + {other}',
                        tearms = self.tearms.extend(other.tearms)).simplify()

    def __sub__(self,other):
        return self + (-1)*other

    __radd__ = __add__
    __rsub__ = __sub__

    def __mul__(self,other):
        data = []
        if type(other) in {int,float,complex}:
            name = f'{other}*({self})'
            for s,bmap in self.tearms:
                data.append((other*s,bmap))

        elif type(other) == type(self):
            data = [tearm_prod(neo) for neo in cart([self.tearms,other.tearms])]
            sn = f'{self}' if len(self.tearms) == 1 else f'({self})'
            on = f'{other}' if len(other.tearms) == 1 else f'({other})'
            name = sn + on
                
        else:
            raise NotImplemented

        return self.set(name = name,
                        tearms = data)
    def __pow__(self,n):
        assert type(n) == int
        assert n >= 0
        name = self.name + f'^{n}'
        running = self
        while n > 1:
            running = running*self 
            n -= 1
        return running.set(name=name)


    __rmul__ = __mul__

    def print_tearms(self):
        list(map(print,self.tearms))

    def simplify(self):
        return self.set(tearms = freeze([(s,bm) for s,bm in self.tearms if s != 0]))

zero_map = State_map(name='0')
One = State_map(tearms = [(1,Name('[.]')(lambda b: (1,b) ))],name='I')

def adag(i):
    @Name(f'[{i}+]')
    def bmap(basis):
        if basis == None or basis[i] == LEVELS - 1 :
            return 1,None
        n = basis[i] 
        basis = list(basis)
        basis[i] = n + 1
        return ((n+1)**0.5,State(basis))
    return State_map(tearms = [(1,bmap)],name = 'a^\dag_{'+str(i)+'}')


def a(i):
    @Name(f'[{i}-]')
    def bmap(basis):
        if basis == None or basis[i] == 0:
            return 1,None
        n = basis[i] 
        basis = list(basis)
        basis[i] = n - 1
        return (n**0.5,State(basis))
    return State_map(tearms = [(1,bmap)],name='a_{'+str(i)+'}')

def n(i):
    return (adag(i)*a(i)).set(name = 'n_{'+str(i)+'}')

def N(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (n(up) + n(do)).set(name=f'N_{i}')

def Z(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (n(up) - n(do)).set(name=f'Z_{i}')

def X(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (adag(up)*a(do) + adag(do)*a(up)).set(name=f'X_{i}')

def Y(i):
    up,do = mode(i,'UP'),mode(i,'DOWN')
    return (-1j*(adag(up)*a(do) - adag(do)*a(up))).set(name=f'Y_{i}')

def mkexp(O):
    return lambda x: expectation(O,x)

def operator_over(O,range,cast = lambda x:x):
    returned = []
    for i in range:
        returned.append(mkexp(cast(O(i))))
    return tuple(returned)
   
def mkcast(basis):
    '''
    cast from the simbolic form defined in this file to
    a numpy form useable in the simulation mecheanary.
    This function dispatches on instance. 
    State_maps -> numpy 2d arrays
    states     -> numpy 1d arrays
    '''

    def cast_vector(state):
        vector = np.zeros(len(basis))
        vector[basis.index(state)] = 1
        return vector

    def cast_matrix(State_map):
        matrix = np.zeros((len(basis),len(basis)),dtype='complex128')
        try:
            for i,pre_image in enumerate(basis):
                values = {}
                for scailer,image in State_map(pre_image):
                    values[image]  = (values.get(image) or 0) + scailer

                globals().update(dict(values = values))

                for image in values:
                    if values[image] != 0:
                        matrix[basis.index(image),i] = values[image]

            return matrix

        except ValueError as e:
            raise Exception(f'Subspace Assumption Violated,{image} not in basis, value tryed to assign {values[image]}')


    dispatch_tabel = {
            'state map': cast_matrix,
            'state': cast_vector,
            }

    def dispatch(state_or_State_map):
        try:
            for t in dispatch_tabel:
                if state_or_State_map._type == t:
                    return dispatch_tabel[t](state_or_State_map)
            else:
                raise Exception(f'Dispatch Error, unable to match type( {state_or_State_map._type} ) to appropreate cast function.')

        except AttributeError:
            raise Exception(f'dispatch only works on data abstractions with a _type attribute')



    return dispatch

# might be nice to but a data abstraction on basis 
# and haave thes as bound methods

def n_sig(b,sig):
    return sum(b[i] for i in range(len(b)) if (i+sig) % 2 == 0)

def _subspace(basis,test):
    return [b for b in basis if test(b)]

def nm_subspace(basis,n,m):
    return _subspace(basis,lambda b: b.number_do() == n and b.number_up() == m)

def N_subspace(basis,N):
    return _subspace(basis,lambda b: b.number_up() + b.number_do() == N)

def average_one_subspace(basis):
    return _subspace(basis,
            lambda b: all(map(
                lambda cite: sum(cite) ==1,
                zip(b[::2],b[1::2]))))


def appropreate_subspace(basis,b):
    return nm_subspace( basis, b.number_do(), b.number_up() )

def one_atom_per_cite(basis):
    return _subspace(basis,
        lambda b: all(sum(cite) == 1
            for cite in zip(b[::2],b[1::2])))



def print_matrix(matrix,basis=None,print=print):

    I = range(len(matrix))
    basis = basis or I

    print('warrning','decimal information forgotten in table formating')
    if np.any(matrix.imag != 0) and np.any(matrix.real != 0):
        formater = lambda n: f'{n:5.0f}'
    else:
        formater = lambda n: f'{n.real + n.imag:2.0f}'
    if np.all(matrix.real == 0) and np.any(matrix.imag != 0):
        print('OVER ALL FACTOR of i')
    if np.any(matrix.real != 0) and np.all(matrix.imag == 0):
        print('compleatly real')

    header = ' '.join(formater(i) for i in I)
    print(header)
    print('_'*len(header))
    for row,lin,b in zip(I,matrix,basis):
        for n in lin:
            c = formater(n)
            if n == 0:
                c = ' '*(len(c)-1) + '.'
            print(c,end=' ')
        print('|',formater(row),'|',b)

def commute(A,B,basis):
    return numpyefy_matrix(A*B  - B*A,basis) == 0

def tsum(*tearms):
    return sum(*tearms,zero_map)

# Initilisation system

def Down_But(ups,CHAINLENGTH):
    '''
    ups is a list of cite indexes to be initialised in the up position
    CHAINLENGTH is the length of the chain, this is requiered to initialise the states
    '''
    state = CHAINLENGTH*[1,0]
    for i in ups:
        state[2*i] = 0
        state[2*i +1] = 1
    return State(state)

def remove(state,ns):
    state = list(state)
    for n in ns:
        state[mode(n,'UP')] = 0
        state[mode(n,'DOWN')] = 0
    return State(state)



#hamiltonions


def H1(U,J,EPSILON, CHAINLENGTH,**perams):
    cites = range(CHAINLENGTH)
    modes = range(2*CHAINLENGTH)
    epsilons_permode = [EPSILON.get(i // 2) or 0 for i in modes]

    H0_sim = tsum(
                epsilons_permode[i] * n(i)
                + (U/2)*n(i)*(One - n(i))
            for i in modes)

    V_sim = -J * tsum(tsum(tsum(adag(mode(i,sigma)) * a(mode(j,sigma))
                     for j in cites if abs(i-j) == 1)
                  for i in cites)
              for sigma in spins)

    return H0_sim + V_sim

def H2(U,J,EPSILON, CHAINLENGTH,**perams):
    cites = range(CHAINLENGTH)
    modes = range(2*CHAINLENGTH)
    epsilons_permode = [EPSILON.get(i // 2) or 0 for i in modes]

    H0_tearms = []
    for i in cites:
        H0_tearms.append( epsilons_permode[i] * n(i)
                        + U*n(mode(i,'UP'))*n(mode(i,'DOWN')))
    H0_sim = tsum(H0_tearms)

    V_sim = -J * tsum(tsum(tsum(adag(mode(i,sigma)) * a(mode(j,sigma))

                     for j in cites if abs(i-j) == 1)
                  for i in cites)
              for sigma in spins)

    return H0_sim + V_sim

def Hisenberge(CHAINLENGTH,Jxyz,**perams):
    Jx,Jy,Jz = Jxyz
    H = -(1/4)* tsum(tsum(
             Jx*X(i)*X(j) + Jy*Y(i)*Y(j) + Jz*Z(i)*Z(j)
             for j in range(CHAINLENGTH) if abs(i-j) == 1)
            for i in range(CHAINLENGTH))
    return H

def anisoH(CHAINLENGTH,alpha,U,eps,**perams):
    H = -(1/4)* tsum(tsum(
             (alpha**2*U*(1/(U - eps[i] + eps[j]) +  1/(U + eps[i] - eps[j])))*\
                (X(i)*X(j) + Y(i)*Y(j) + Z(i)*Z(j))
             for j in range(CHAINLENGTH) if abs(i-j) == 1)
            for i in range(CHAINLENGTH))
    return H



def XX(CHAINLENGTH,Jxyz,**perams):
    Jx,Jy,Jz = Jxyz
    H = -(1/4)* tsum(tsum(
             Jx*X(i)*X(j) + Jy*Y(i)*Y(j)
             for j in range(CHAINLENGTH) if abs(i-j) == 1)
            for i in range(CHAINLENGTH))
    return H


provided_hamiltonions = {
        '1': H1,
        'standad hubbard': H1,
        '2': H2,
        'HIS': Hisenberge,
        'hisenberge': Hisenberge,
        'aniso': anisoH,
        'xx' : XX,
        }


# Unitary system

def Un(H,time):
    dts = time - np.roll(time,1)
    dt = dts[1]
    assert np.all(dt - dts[1:] < 1e-10 ), 'Only valid for a time lattice'

    _Un = U1 = linalg.expm(-1j*dt*H)
    for t in time:
        _Un = (U1@_Un)
        yield _Un

def U_direct(H,time):
    for t in time:
        yield linalg.expm(-1j*t*H)

provided_unitary_methods = {
    '1': U_direct, 
    'direct': U_direct,
    '2': Un,
    'itteration': Un,
    }


# State generation system

def Compute_States(Phi0,Us,prog=None):
    for t,U in enumerate(Us):
        if prog != None: prog(t)
        yield U@Phi0


