#!/bin/env python

import simulator as sim
import numpy as np
from helpers import Acuumulator

scope = None

def comparision_data(
        U = 1,
        alpha = 0.1, # alpha = J / U
        Delta = 0.01,
        time = np.arange(0,1000,0.1),
        CHAINLENGTH = 3,
        ):


    initial = sim.Down_But([0],CHAINLENGTH)
    basis_hub = sim.appropreate_subspace(sim.brange(CHAINLENGTH),initial)
    basis_his = sim.one_atom_per_cite(basis_hub)

    #epsilons = {n:((-0.5)**n)*Delta for n in range(CHAINLENGTH)}
    epsilons = {0:Delta}

    Jeff = 2*alpha**2

    ham_hub = sim.provided_hamiltonions['2'](U,alpha*U,epsilons,CHAINLENGTH)
    ham_his = sim.provided_hamiltonions['hisenberge'](CHAINLENGTH,3*[Jeff])

    cast_hub = sim.mkcast(basis_hub)
    cast_his = sim.mkcast(basis_his)

    Initial_hub = cast_hub(initial)
    Initial_his = cast_his(initial)

    print('casting')
    Ham_hub = cast_hub(ham_hub)
    Ham_his = cast_his(ham_his)

    print('Hubbard Hamiltonian')
    sim.print_matrix(Ham_hub,basis_hub)

    print('Hisenberg Hamiltonian')
    sim.print_matrix(Ham_his,basis_his)

    print('defining observer operators')

    Op_Ns_hub = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast_hub)
    Op_Zs_hub = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast_hub)
    
    Op_Ns_his = sim.operator_over(sim.N,range(CHAINLENGTH),cast = cast_his)
    Op_Zs_his = sim.operator_over(sim.Z,range(CHAINLENGTH),cast = cast_his)

    U = sim.provided_unitary_methods['itteration']

    Sim_hub = sim.Compute_States(Initial_hub,U(Ham_hub,time))
    Sim_his = sim.Compute_States(Initial_his,U(Ham_his,time))
    
    data_hub = Acuumulator()
    data_his = Acuumulator()
    for t,State_hub,State_his in zip(time,Sim_hub,Sim_his):
        if t % 5 == 0:
            print(f'-> {t:0.2f}',end='\r')

        data_hub(
                Ns = [ N(State_hub) for N in Op_Ns_hub ],
                Ms = [ M(State_hub) for M in Op_Zs_hub ],
                )

        data_his(
                Ns = [ N(State_his) for N in Op_Ns_his ],
                Ms = [ M(State_his) for M in Op_Zs_his ],
                )

    scope = locals()
    return time,data_hub.arraysT(),data_his.arraysT()


def phft(time,td_hub,td_his):
    'posative half furia transform'

    freq = np.fft.fftfreq( n = len(time), d = time[1] - time[0] )
    frange = freq > 0
    freq = freq[frange]

    def fft(array):
        return abs(np.fft.fft(array)[frange])

    fd_hub = Acuumulator()
    fd_his = Acuumulator()
    for cite in range(len(td_hub[0])):
        fd_hub( Ns = fft( td_hub[0][cite] ),
                Ms = fft( td_hub[1][cite] ))

        fd_his( Ns = fft( td_his[0][cite] ),
                Ms = fft( td_his[1][cite] ))

    return freq,fd_hub.arrays(),fd_his.arrays()

def align_factor(freq,M_hub,M_his):
    f_peaks_hub = [ freq[ M == M.max() ][0] for M in M_hub ]
    f_peaks_his = [ freq[ M == M.max() ][0] for M in M_his ]

    f_peak_hub = f_peaks_hub[0]
    f_peak_his = f_peaks_his[0]

    assert np.all(f_peaks_hub == f_peaks_hub),str(f_peaks_hub)
    assert np.all(f_peaks_his == f_peaks_his),str(f_peaks_his)

    print(f_peaks_hub,f_peaks_his)

    return f_peak_his/f_peak_hub

def factor(**config):
    freq, phft_hub, phft_his = phft(*comparision_data(**config))
    factor = align_factor(freq,phft_hub[1],phft_his[1])

    return factor,freq[0]


import matplotlib.pyplot as plt
def plot(index1,index2,data_hub,data_his,range1,range2):
    index1 = index1[range1]
    index2 = index2[range2]

    Ns_hub, Ms_hub = data_hub
    Ns_his, Ms_his = data_his

    CHAINLENGTH = len(Ns_hub)

    N_tot_hub = np.sum(Ns_hub,axis=0)
    M_tot_hub = np.sum(Ms_hub,axis=0)

    N_tot_his = np.sum(Ns_his,axis=0)
    M_tot_his = np.sum(Ms_his,axis=0)

    fig,axs = plt.subplots(CHAINLENGTH+1,2,sharey=False)
    axsl,axsr = axs.T

    for i,ax in enumerate(axsl):
        ax.grid()
        if i == 0:
            ax.set_title('Hubbard Model')
        if i == CHAINLENGTH:
            ax.plot(index1,N_tot_hub[range1])
            ax.plot(index1,M_tot_hub[range1])
            continue

        ax.plot(index1,Ns_hub[i][range1])
        ax.plot(index1,Ms_hub[i][range1])

    for i,ax in enumerate(axsr):
        ax.grid()
        if i == 0:
            ax.set_title('Heisenberg Model')
        if i == CHAINLENGTH:
            ax.plot(index2,N_tot_his[range2])
            ax.plot(index2,M_tot_his[range2])
            continue

        ax.plot(index2,Ns_his[i][range2])
        ax.plot(index2,Ms_his[i][range2])

if __name__ == '__main__':

    time, data_hub, data_his = comparision_data()
    freq, phft_hub, phft_his = phft(time, data_hub, data_his)
    factor = align_factor(freq,phft_hub[1],phft_his[1])

    range1 = time < 250
    range2 = (factor*time) < 250

    rangefreq1 = freq < 0.2
    rangefreq2 = freq < 0.2
    
    plot(freq, freq, phft_hub, phft_his, rangefreq2, rangefreq1)
    plt.tight_layout()
    plt.savefig('results/emergent/new2.pdf')
    
    plot(time, factor*time, data_hub, data_his, range1, range2)
    plt.tight_layout()
    plt.savefig('results/emergent/new1.pdf')

    plt.show()
