import numpy as np

class Acuumulator:
    def __init__(self):
        self.keys = []
        self.vals = []

    def index(self,key):
        return self.keys.index(key)

    def __call__(self,**dic):
        for k in dic:
            if k not in self.keys:
                self.keys.append(k)
                self.vals.append([])
            index = self.index(k)
            self.vals[index].append(dic[k])

    def arrays(self,keys = None):
        keys = keys or self.keys
        returns = []
        for k in keys:
            index = self.index(k)
            returns.append(np.array(self.vals[index]))
        return returns

    def arraysT(self,keys = None):
        keys = keys or self.keys
        returns = self.arrays(keys=keys)
        for i in range(len(returns)):
            returns[i] = returns[i].T
        return returns

    def dic(self,keys = None):
        keys = keys or self.keys
        returns = {}
        for k in keys:
            index = self.index(k)
            returns.update({k:self.vals[index]})
        return returns
        
    def __repr__(self):
        string = ''
        for k in self.keys:
            index = self.index(k)
            string += str(k) + '\t'
            string += str(self.vals[index])
            string += '\n'
        return string

    def get(self,key):
        return self.vals[self.index(key)]
 

def trunkate(*ls):
    ''' give me some lists, I will trunkate them so that they all have the length of the sortest list'''
    L = min([len(l) for l in ls])
    ls = [l[:L] for l in ls]
    return ls
