#!/bin/bash
Scode="S3"
echo Series $Scode
echo "(Vary U)" x "(Const J)" x "(epsilon = 0)"  x "(one attom missing)"

file="results/$Scode/"
mkdir -p $file

base="MODEL_NAME $Scode\
        WRITE_STATES True\
        CHAINLENGTH 5\
        LEVELS 2\
        HAM 2\
        SUBSPACE True\
        TIME_LEN 100\
        PLOT 3\
        SHOWPLOT False\
        REMOVE [1]\
        "

echo $base

J=10
for U in `seq 0 1000`; do
	echo $U $J
	printf -v n "%05d" $U
	output=$file/$n-J$J
	python3 hubbard.py $base OUTPUT $output U $U J $J;
	echo takeing fft
	python3 fft.py $output
done

echo ---------------
echo CREATING VIDIOS
echo ---------------
echo creating time domain 
v1=~/public_html/$Scode.mp4
rm $v1
yes | ffmpeg -framerate 30 -pattern_type glob -i "$file/**/n*.png" -c:v libx264 $v1
echo creating frequancy domain 
v2=$Scode-fft.mp4
rm $v2
yes | ffmpeg -framerate 30 -pattern_type glob -i "$file/**/fft.png" -c:v libx264 $v2
