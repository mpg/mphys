#!/bin/bash
Scode="S2"
echo Series $Scode
echo "(U = 300)" x "(J = 10)" x "(Vary epsilon)"


file="results/$Scode/"
mkdir -p $file

base="MODEL_NAME $Scode \
        WRITE_STATES True\
        CHAINLENGTH 5\
        LEVELS 2\
        HAM 2\
        SUBSPACE True\
        TIME_LEN 20\
        PLOT 3\
        SHOWPLOT False\
        "

echo $base

J=10
U=100
for i in `seq 0 500`; do
	echo $i $U $J
	echo generating data
	printf -v n "%05d" $i
	e=$(python3 -c "print(-$i/5)")
	output=$file/i$n
	python3 hubbard.py $base OUTPUT $output U $U J $J EPSILON "{1:$e}";
	echo takeing fft
	python3 fft.py $output
done

echo ---------------
echo CREATING VIDIOS
echo ---------------
echo creating time domain 
v1=~/public_html/$Scode.mp4
rm $v1
yes | ffmpeg -framerate 30 -pattern_type glob -i "$file/**/n*.png" -c:v libx264 $v1
echo creating frequancy domain 
v2=$Scode-fft.mp4
rm $v2
yes | ffmpeg -framerate 30 -pattern_type glob -i "$file/**/fft.png" -c:v libx264 $v2
